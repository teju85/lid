var currTestQuestionId = 0;
var questionIdsLocal = [];

function onLoad() {
    currTestQuestionId = 0;
    clearStats();
    questionIdsLocal = generateQuestionIds();
    displayQuestion(questionIdsLocal[currTestQuestionId]);
}

function checkAnswer(choice) {
    if (currTestQuestionId < NumQuestionsInTest) {
        isCorrect(questionIdsLocal[currTestQuestionId], choice);
        displayStats();
        currTestQuestionId++;
        clearAnswers();
        displayQuestion(questionIdsLocal[currTestQuestionId]);
    }
    if (currTestQuestionId >= NumQuestionsInTest) {
        var status = document.getElementById("status");
        if (numCorrect >= MinCorrectAnswers) {
            status.innerHTML = `Result: ${StatusEmoji[0]}`;
        } else {
            status.innerHTML = `Result: ${StatusEmoji[1]}`;
        }
    }
}
