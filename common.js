const StatusEmoji = [
    "&#x1F600;",  // correct
    "&#x1F622;"   // incorrect
];
const StatusSymbols = [
    "&#x2705;",   // correct
    "&#x274C;"    // incorrect
];
const NumCommonQuestions = 300;
const NumPerStateQuestions = 10;
const NumTotalQuestions = NumCommonQuestions + NumPerStateQuestions;
const NumQuestionsInTest = 33;
const MinCorrectAnswers = 17;
const States = [
    "Baden-Württemberg",
    "Bayern",
    "Berlin",
    "Brandenburg",
    "Bremen",
    "Hamburg",
    "Hessen",
    "Mecklenburg-Vorpommern",
    "Niedersachsen",
    "Nordrhein-Westfalen",
    "Rheinland-Pfalz",
    "Saarland",
    "Sachsen",
    "Sachsen-Anhalt",
    "Schleswig-Holstein",
    "Thüringen"
];

var numCorrect = 0;
var numIncorrect = 0;
var stateId = 1;  // Bayern

function changeState() {
    var state = document.getElementById("states").value;
    stateId = parseInt(state, 10);
}

function global2local(gid) {
    if (gid <= NumCommonQuestions) return gid;
    return gid - (stateId * NumPerStateQuestions);
}

function local2global(lid) {
    if (lid <= NumCommonQuestions) return lid;
    return lid + (stateId * NumPerStateQuestions);
}

function displayQuestion(id) {
    var gid = local2global(id);
    var ques = questions[gid + ""];
    document.getElementById("question-id").innerHTML = `Question #${id}`;
    document.getElementById("question").innerHTML = ques["q"];
    for (var i = 1; i <= 4; ++i) {
        document.getElementById("C" + i).innerHTML = ques["c"][i - 1];
    }
    clearAnswers();
}

function clearAnswers() {
    var ele = document.getElementsByName("answer");
    for (var i = 0; i < ele.length; i++) {
        ele[i].checked = false;
    }
    var status = document.getElementById("status");
    status.innerHTML = "";
    var answer = document.getElementById("answer");
    if (answer != null) {
        answer.innerHTML = "";
    }
}

function clearStats() {
    numCorrect = 0;
    numIncorrect = 0;
    displayStats();
}

function displayStats() {
    var stats = document.getElementById("stats");
    stats.innerHTML = `${StatusSymbols[0]}: ${numCorrect} &nbsp;&nbsp; ${StatusSymbols[1]}: ${numIncorrect}`;
}

function randInt(start, end) {
    let diff = end - start;
    let num = Math.floor(Math.random() * diff);
    return num + start;
}

function generateQuestionIds() {
    var questionIds = [];
    for (var i = 0; i < NumQuestionsInTest; ++i) {
        questionIds.push(randInt(1, NumTotalQuestions + 1));
    }
    return questionIds;
}

function isCorrect(id, choice) {
    var correct = correctAnswerFor(id) == choice;
    if (correct) {
        numCorrect++;
    } else {
        numIncorrect++;
    }
    return correct;
}

function correctAnswerFor(id) {
    return answers[local2global(id)];
}
