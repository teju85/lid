var currQuestionIdLocal = 1;

function onLoad() {
    currQuestionIdLocal = 1;
    clearStats();
    displayQuestion(currQuestionIdLocal);
}

function prevQuestion() {
    if (currQuestionIdLocal == 1) return;
    --currQuestionIdLocal;
    displayQuestion(currQuestionIdLocal);
}

function nextQuestion() {
    if (currQuestionIdLocal == NumTotalQuestions) return;
    ++currQuestionIdLocal;
    displayQuestion(currQuestionIdLocal);
}

function checkAnswer(choice) {
    var status = document.getElementById("status");
    var answer = document.getElementById("answer");
    if (isCorrect(currQuestionIdLocal, choice)) {
        status.innerHTML = StatusEmoji[0];
        answer.innerHTML = "";
    } else {
        status.innerHTML = StatusEmoji[1];
        var correct = correctAnswerFor(currQuestionIdLocal);
        answer.innerHTML = `Die richtige Antwort ist: ${correct}`;
    }
    displayStats();
}
