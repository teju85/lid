var questions = {
    "1": {
        "q": "In Deutschland dürfen Menschen offen etwas gegen die Regierung sagen, weil",
        "c": [
            "hier Religionsfreiheit gilt.",
            "die Menschen Steuern zahlen.",
            "die Menschen das Wahlrecht haben.",
            "hier Meinungsfreiheit gilt."
        ]
    },
    "2": {
        "q": "In Deutschland können Eltern bis zum 14. Lebensjahr ihres Kindes entscheiden, ob es in der Schule am",
        "c": [
            "Geschichtsunterricht teilnimmt.",
            "Religionsunterricht teilnimmt.",
            "Politikunterricht teilnimmt.",
            "Sprachunterricht teilnimmt."
        ]
    },
    "3": {
        "q": "Deutschland ist ein Rechtsstaat. Was ist damit gemeint?",
        "c": [
            "Alle Einwohner / Einwohnerinnen und der Staat müssen sich an die Gesetze halten.",
            "Der Staat muss sich nicht an die Gesetze halten.",
            "Nur Deutsche müssen die Gesetze befolgen.",
            "Die Gerichte machen die Gesetze."
        ]
    },
    "4": {
        "q": "Welches Recht gehört zu den Grundrechten in Deutschland?",
        "c": [
            "Waffenbesitz",
            "Faustrecht",
            "Meinungsfreiheit",
            "Selbstjustiz"
        ]
    },
    "5": {
        "q": "Wahlen in Deutschland sind frei. Was bedeutet das?",
        "c": [
            "Man darf Geld annehmen, wenn man dafür einen bestimmten Kandidaten / eine bestimmte Kandidatin wählt.",
            "Nur Personen, die noch nie im Gefängnis waren, dürfen wählen.",
            "Der Wähler darf bei der Wahl weder beeinflusst noch zu einer bestimmten Stimmabgabe gezwungen werden und keine Nachteile durch die Wahl haben.",
            "Alle wahlberechtigten Personen müssen wählen."
        ]
    },
    "6": {
        "q": "Wie heißt die deutsche Verfassung?",
        "c": [
            "Volksgesetz",
            "Bundesgesetz",
            "Deutsches Gesetz",
            "Grundgesetz"
        ]
    },
    "7": {
        "q": "Welches Recht gehört zu den Grundrechten, die nach der deutschen Verfassung garantiert werden? Das Recht auf",
        "c": [
            "Glaubens- und Gewissensfreiheit.",
            "Unterhaltung.",
            "Arbeit.",
            "Wohnung."
        ]
    },
    "8": {
        "q": "Was steht nicht im Grundgesetz von Deutschland?",
        "c": [
            "Die Würde des Menschen ist unantastbar.",
            "Alle sollen gleich viel Geld haben.",
            "Jeder Mensch darf seine Meinung sagen.",
            "Alle sind vor dem Gesetz gleich."
        ]
    },
    "9": {
        "q": "Welches Grundrecht gilt in Deutschland nur für Ausländer / Ausländerinnen? Das Grundrecht auf",
        "c" : [
            "Schutz der Familie",
            "Menschenwürde",
            "Asyl",
            "Meinungsfreiheit",
        ]
    },
    "10": {
        "q": "Was ist mit dem deutschen Grundgesetz vereinbar?",
        "c": [
            "die Prügelstrafe",
            "die Folter",
            "die Todesstrafe",
            "die Geldstrafe"
        ]
    },
    "11": {
        "q": "Wie wird die Verfassung der Bundesrepublik Deutschland genannt?",
        "c": [
            "Grundgesetz",
            "Bundesverfassung",
            "Gesetzbuch",
            "Verfassungsvertrag"
        ]
    },
    "12": {
        "q": "Eine Partei im Deutschen Bundestag will die Pressefreiheit abschaffen. Ist das moglich?",
        "c": [
            "Ja, wenn mehr als die Hälfte der Abgeordneten im Bundestag dafür sind.",
            "Ja, aber dazu müssen zwei Drittel der Abgeordneten im Bundestag dafür sein.",
            "Nein, denn die Pressefreiheit ist ein Grundrecht. Sie kann nicht abgeschafft werden.",
            "Nein, denn nur der Bundesrat kann die Pressefreiheit abschaffen."
        ]
    },
    "13": {
        "q": "Im Parlament steht der Begriff 'Opposition' für",
        "c": [
            "die regierenden Parteien.",
            "die Fraktion mit den meisten Abgeordneten.",
            "alle Parteien, die bei der letzten Wahl die 5%-Hürde erreichen konnten.",
            "alle Abgeordneten, die nicht zu der Regierungspartei/den Regierungsparteien gehören."
        ]
    },
    "14": {
        "q": "Meinungsfreiheit in Deutschland heißt, dass ich",
        "c": [
            "Passanten auf der Straße beschimpfen darf.",
            "meine Meinung in Leserbriefen äußern kann.",
            "Nazi-Symbole tragen darf.",
            "meine Meinung sagen darf, solange ich der Regierung nicht widerspreche."
        ]
    },
    "15": {
        "q": "Was verbietet das deutsche Grundgesetz?",
        "c": [
            "Militärdienst",
            "Zwangsarbeit",
            "freie Berufswahl",
            "Arbeit im Ausland"
        ]
    },
    "16": {
        "q": "Wann ist die Meinungsfreiheit in Deutschland eingeschränkt?",
        "c": [
            "bei der öffentlichen Verbreitung falscher Behauptungen über einzelne Personen",
            "bei Meinungsäußerungen über die Bundesregierung",
            "bei Diskussionen über Religionen",
            "bei Kritik am Staat"
        ]
    },
    "17": {
        "q": "Die deutschen Gesetze verbieten",
        "c": [
            "Meinungsfreiheit der Einwohner und Einwohnerinnen.",
            "Petitionen der Bürger und Bürgerinnen.",
            "Versammlungsfreiheit der Einwohner und Einwohnerinnen.",
            "Ungleichbehandlung der Bürger und Bürgerinnen durch den Staat."
        ]
    },
    "18": {
        "q": "Welches Grundrecht ist in Artikel 1 des Grundgesetzes der Bundesrepublik Deutschland garantiert?",
        "c": [
            "die Unantastbarkeit der Menschenwürde",
            "das Recht auf Leben",
            "Religionsfreiheit",
            "Meinungsfreiheit"
        ]
    },
    "19": {
        "q": "Was versteht man unter dem Recht der 'Freizügigkeit' in Deutschland?",
        "c": [
            "Man darf sich seinen Wohnort selbst aussuchen.",
            "Man kann seinen Beruf wechseln.",
            "Man darf sich für eine andere Religion entscheiden.",
            "Man darf sich in der Öffentlichkeit nur leicht bekleidet bewegen."
        ]
    },
    "20": {
        "q": "Eine Partei in Deutschland verfolgt das Ziel, eine Diktatur zu errichten. Sie ist dann",
        "c": [
            "tolerant.",
            "rechtsstaatlich orientiert.",
            "gesetzestreu.",
            "verfassungswidrig."
        ]
    },
    "21": {
        "q": "Welches ist das Wappen der Bundesrepublik Deutschland? <br><img src='./images/wappen.png'>",
        "c": [
            "1",
            "2",
            "3",
            "4"
        ]
    },
    "22": {
        "q": "Was für eine Staatsform hat Deutschland?",
        "c": [
            "Monarchie",
            "Diktatur",
            "Republik",
            "Fürstentum"
        ]
    },
    "23": {
        "q": "In Deutschland sind die meisten Erwerbstätigen",
        "c": [
            "in kleinen Familienunternehmen beschäftigt.",
            "ehrenamtlich für ein Bundesland tätig.",
            "selbständig mit einer eigenen Firma tätig.",
            "bei einer Firma oder Behörde beschäftigt."
        ]
    },
    "24": {
        "q": "Wie viele Bundesländer hat die Bundesrepublik Deutschland?",
        "c": [
            "14",
            "15",
            "16",
            "17"
        ]
    },
    "25": {
        "q": "Was ist kein Bundesland der Bundesrepublik Deutschland?",
        "c": [
            "Elsass-Lothringen",
            "Nordrhein-Westfalen",
            "Mecklenburg-Vorpommern",
            "Sachsen-Anhalt"
        ]
    },
    "26": {
        "q": "Deutschland ist",
        "c": [
            "eine kommunistische Republik.",
            "ein demokratischer und sozialer Bundesstaat.",
            "eine kapitalistische und soziale Monarchie.",
            "ein sozialer und sozialistischer Bundesstaat."
        ]
    },
    "27": {
        "q": "Deutschland ist",
        "c": [
            "ein sozialistischer Staat.",
            "ein Bundesstaat.",
            "eine Diktatur.",
            "eine Monarchie."
        ]
    },
    "28": {
        "q": "Wer wählt in Deutschland die Abgeordeneten zum Bundestag?",
        "c": [
            "das Militär",
            "die Wirtschaft",
            "das wahlberechtigte Volk",
            "die Verwaltung"
        ]
    },
    "29": {
        "q": "Welches Tier ist das Wappentier der Bundesrepublik Deutschland?",
        "c": [
            "Löwe",
            "Adler",
            "Bär",
            "Pferd"
        ]
    },
    "30": {
        "q": "Was ist kein Merkmal unserer Demokratie?",
        "c": [
            "regelmäßige Wahlen",
            "Pressezensur",
            "Meinungsfreiheit",
            "verschiedene Parteien"
        ]
    },
    "31": {
        "q": "Die Zusammenarbeit von Parteien zur Bildung einer Regierung nennt man in Deutschland",
        "c": [
            "Einheit.",
            "Koalition.",
            "Ministerium.",
            "Fraktion."
        ]
    },
    "32": {
        "q": "Was ist keine staatliche Gewalt in Deutschland?",
        "c": [
            "Gesetzgebung",
	    "Regierung",
	    "Presse",
	    "Rechtsprechung"
        ]
    },
    "33": {
        "q": "Welche Aussage is richtig? In Deutschland",
        "c": [
            "sind Staat und Religionsgemeinschaften voneinander getrennt.",
            "bilden die Religionsgemeinschaften den Staat.",
            "ist der Staat abhängig von den Religionsgemeinschaften.",
            "bilden Staat und Religionsgemeinschaften eine Einheit."
        ]
    },
    "34": {
        "q": "Was ist Deutschland nicht?",
        "c": [
            "eine Demokratie",
	    "ein Rechtsstaat",
	    "eine Monarchie",
            "ein Sozialstaat"
        ]
    },
    "35": {
        "q": "Womit finanziert der deutsche Staat die Sozialversicherung?",
        "c": [
            "Kirchensteuern",
	    "Sozialabgaben",
	    "Spendengeldern",
	    "Vereinsbeiträgen"
        ]
    },
    "36": {
        "q": "Welche Maßnahme schafft in Deutschland soziale Sicherheit?",
        "c": [
            "die Krankenversicherung",
            "die Autoversicherung",
            "die Gebäudeversicherung",
            "die Haftpflichtversicherung"
        ]
    },
    "37": {
        "q": " Wie werden die Regierungschefs / Regierungschefins der meisten Bundesländer genannt?",
        "c": [
"Erster Minister/ Erste Ministerin",
            "Premierminister/ Premierministerin",
            "Senator/ Senatorin",
            "Ministerpräsident/ Ministerpräsidentin"
        ]
    },
    "38": {
        "q": "Die Bundesrepublik Deutschland ist ein demokratischer und sozialer",
        "c": [
            "Staatenverbund.",
	    "Bundesstaat.",
	    "Staatenbund.",
            "Zentralstaat."
        ]
    },
    "39": {
        "q": "Was hat jedes deutsche Bundesland?",
        "c": [
            "einen eigenen Außenminister / eine eigene Außenministerin",
	    "eine eigene Währung",
	    "eine eigene Armee",
	    "eine eigene Regierung"
        ]
    },
    "40": {
        "q": "Mit welchen Worten beginnt die deutsche Nationalhymne?",
        "c": [
            "Völker, hört die Signale ...",
	    "Einigkeit und Recht und Freiheit ...",
	    "Freude schöner Götterfunken ...",
	    "Deutschland einig Vaterland ..."
        ]
    },
    "41": {
        "q": "Warum gibt es in einer Demokratie mehr als eine Partei?",
        "c": [
            "weil dadurch die unterschiedlichen Meinungen der Bürger und Bürgerinnen vertreten werden",
	    "damit Bestechung in der Politik begrenzt wird",
	    "um politische Demonstrationen zu verhindern",
	    "um wirtschaftlichen Wettbewerb anzuregen"
        ]
    },
    "42": {
        "q": "Wer beschließt in Deutschland ein neues Gesetz?",
        "c": [
            "die Regierung",
	    "das Parlament",
	    "die Gerichte",
	    "die Polizei"
        ]
    },
    "43": {
        "q": "Wann kann in Deutschland eine Partei verboten werden?",
        "c": [
            "wenn ihr Wahlkampf zu teuer ist",
	    "wenn sie gegen die Verfassung kämpft",
	    "wenn sie Kritik am Staatsoberhaupt äußert",
	    "wenn ihr Programm eine neue Richtung vorschlägt"
        ]
    },
    "44": {
        "q": "Wenn kann man als Bürger / Bürgerin in Deutschland nicht direkt wählen?",
        "c": [
            "Abgeordnete des EU-Parlaments",
	    "den Bundespräsidenten / die Bundespräsidentin",
	    "Landtagsabgeordnete",
	    "Bundestagsabgeordnete"
        ]
    },
    "45": {
        "q": "Zu welcher Versicherung gehört die Pflegeversicherung?",
        "c": [
            "Sozialversicherung",
	    "Unfallversicherung",
	    "Hausratversicherung",
	    "Haftpflicht- und Feuerversicherung"
        ]
    },
    "46": {
        "q": "Der deutsche Staat hat viele Aufgaben. Welche Aufgabe gehört dazu?",
        "c": [
            "Er baut Straßen und Schulen.",
	    "Er verkauft Lebensmittel und Kleidung.",
	    "Er versorgt alle Einwohner und Einwohnerinnen kostenlos mit Zeitungen.",
	    "Er produziert Autos und Busse."
        ]
    },
    "47": {
        "q": "Der deutsche Staat hat viele Aufgaben. Welche Aufgabe gehört nicht dazu?",
        "c": [
            "Er bezahlt für alle Staatsangehörigen Urlaubsreisen.",
	    "Er zahlt Kindergeld.",
	    "Er unterstützt Museen.",
	    "Er fördert Sportler und Sportlerinnen."
        ]
    },
    "48": {
        "q": "Welches Organ gehört nicht zu den Verfassungsorganen Deutschlands?",
        "c": [
            "der Bundesrat",
	    "der Bundespräsident / die Bundespräsidentin",
	    "die Bürgerversammlung",
	    "die Regierung"
        ]
    },
    "49": {
        "q": "Wer bestimmt in Deutschland die Schulpolitik?",
        "c": [
            "die Lehrer und Lehrerinnen",
	    "die Bundesländer",
	    "das Familienministerium",
	    "die Universitäten"
        ]
    },
    "50": {
        "q": "Die Wirtschaftsform in Deutschland nennt man",
        "c": [
            "reie Zentralwirtschaft.",
	    "soziale Marktwirtschaft.",
	    "gelenkte Zentralwirtschaft.",
	    "Planwirtschaft."
        ]
    },
    "51": {
        "q": "Zu einem demokratischen Rechtsstaat gehört es nicht, dass",
        "c": [
            "Menschen sich kritisch über die Regierung äußern können.",
	    "Bürger friedlich demonstrieren gehen dürfen.",
	    "Menschen von einer Privatpolizei ohne Grund verhaftet werden.",
	    "jemand ein Verbrechen begeht und deshalb verhaftet wird."
        ]
    },
    "52": {
        "q": "Was bedeutet 'Volkssouveränität'? Alle Staatsgewalt geht vom",
        "c": [
            "Volke aus.",
	    "Bundestag aus.",
	    "preußischen König aus.",
	    "Bundesverfassungsgericht aus."
        ]
    },
    "53": {
        "q": "Was bedeutet 'Rechtsstaat' in Deutschland?",
        "c": [
            "Der Staat hat Recht.",
	    "Es gibt nur rechte Parteien.",
	    "Die Bürger und Bürgerinnen entscheiden über Gesetze.",
	    "Der Staat muss die Gesetze einhalten."
        ]
    },
    "54": {
        "q": "Was ist keine staatliche Gewalt in Deutschland?",
        "c": [
            "Legislative",
	    "Judikative",
	    "Exekutive",
	    "Direktive"
        ]
    },
    "55": {
        "q": "Wie zeigt dieses Bild? <br><img src='./images/building.png'>",
        "c": [
            "den Bundestagssitz in Berlin",
	    "das Bundesverfassungsgericht in Karlsruhe",
	    "das Bundesratsgebäude in Berlin",
	    "das Bundeskanzleramt in Berlin"
        ]
    },
    "56": {
        "q": "Welches Amt gehört in Deutschland zur Gemeindeverwaltung?",
        "c": [
            "Pfarramt",
	    "Ordnungsamt",
	    "Finanzamt",
	    "Auswärtiges Amt"
        ]
    },
    "57": {
        "q": "Wer wird meistens zum Präsidenten / zur Präsidentin des Deutschen Bundestags gewaehlt?",
        "c": [
            "der / die älteste Abgeordnete im Parlament",
	    "der Ministerpräsident / die Ministerpräsidentin des größten Bundeslandes",
	    "ein ehemaliger Bundeskanzler / eine ehemalige Bundeskanzlerin",
	    "ein Abgeordneter / eine Abgeordnete der stärksten Fraktion"
        ]
    },
    "58": {
        "q": "Wer ernennt in Deutschland die Minister / die Ministerinnen der Bundesregierung?",
        "c": [
            "der Präsident / die Präsidentin des Bundesverfassungsgerichtes",
	    "der Bundespräsident / die Bundespräsidentin",
	    "der Bundesratspräsident / die Bundesratspräsidentin",
	    "der Bundestagspräsident / die Bundestagspräsidentin"
        ]
    },
    "59": {
        "q": "Welche Parteien wurden in Deutschland 2007 zur Partei 'Die Linke'?",
        "c": [
            "CDU und SSW",
	    "PDS und WASG",
	    "CSU und FDP",
	    "Bündnis 90/Die Grünen und SPD"
        ]
    },
    "60": {
        "q": "In Deutschland gehören der Bundestag und der Bundesrat zur",
        "c": [
            "Exekutive.",
	    "Legislative.",
	    "Direktive.",
	    "Judikative."
        ]
    },
    "61": {
        "q": "Was bedeutet 'Volkssouveränität'?",
        "c": [
            "Der König / die Königin herrscht über das Volk.",
	    "Das Bundesverfassungsgericht steht über der Verfassung.",
	    "Die Interessenverbände üben die Souveränität zusammen mit der Regierung aus.",
	    "Die Staatsgewalt geht vom Volke aus."
        ]
    },
    "62": {
        "q": "Wenn das Parlament eines deutschen Bundeslandes gewählt wird, nennt man das",
        "c": [
            "Kommunalwahl.",
	    "Landtagswahl.",
	    "Europawahl.",
	    "Bundestagswahl."
        ]
    },
    "63": {
        "q": "Was gehört in Deutschland nicht zur Exekutive?",
        "c": [
            "die Polizei",
	    "die Gerichte",
	    "das Finanzamt",
	    "die Ministerien"
        ]
    },
    "64": {
        "q": "Die Bundesrepublik Deutschland ist heute gegliedert in",
        "c": [
            "vier Besatzungszonen.",
	    "einen Oststaat und einen Weststaat.",
	    "16 Kantone.",
	    "Bund, Länder und Kommunen."
        ]
    },
    "65": {
        "q": "Es gehört nicht zu den Aufgaben des Deutschen Bundestages",
        "c": [
            "Gesetze zu entwerfen.",
	    "die Bundesregierung zu kontrollieren.",
	    "den Bundeskanzler / die Bundeskanzlerin zu wählen.",
	    "das Bundeskabinett zu bilden."
        ]
    },
    "66": {
        "q": "Wer schrieb den Text zur deutschen Nationalhymne?",
        "c": [
            "Friedrich von Schiller",
	    "Clemens Brentano",
	    "Johann Wolfgang von Goethe",
	    "Heinrich Hoffmann von Fallersleben"
        ]
    },
    "67": {
        "q": "Was ist in Deutschland vor allem eine Aufgabe der Bundesländer?",
        "c": [
            "Verteidigungspolitik",
	    "Außenpolitik",
	    "Wirtschaftspolitik",
	    "Schulpolitik"
        ]
    },
    "68": {
        "q": "Warum kontrolliert der Staat in Deutschland das Schulwesen?",
        "c": [
            "weil es in Deutschland nur staatliche Schulen gibt",
	    "weil alle Schüler und Schülerinnen einen Schulabschluss haben müssen",
	    "weil es in den Bundesländern verschiedene Schulen gibt",
	    "weil es nach dem Grundgesetz seine Aufgabe ist"
        ]
    },
    "69": {
        "q": "Die Bundesrepublik Deutschland hat einen dreistufigen Verwaltungsaufbau. Wie heißt die unterste politische Stufe",
        "c": [
            "Stadträte",
	    "Landräte",
	    "Gemeinden",
	    "Bezirksämter"
        ]
    },
    "70": {
        "q": "Der deutsche Bundespräsident Gustav Heinemann gibt Helmut Schmidt 1974 die Ernennungsurkunde zum deutschen Bundeskanzler. Was gehört zu gen Aufgaben des deutschen Bundespräsidenten / der deutschen Bundespräsidentin? <br><img src='./images/gustav.png'>",
        "c": [
            "Er / Sie führt die Regierungsgeschäfte.",
	    "Er / Sie kontrolliert die Regierungspartei.",
	    "Er / Sie wählt die Minister / Ministerinnen aus.",
	    "Er / Sie schlägt den Kanzler / die Kanzlerin zur Wahl vor."
        ]
    },
    "71": {
        "q": "Wo hält sich der deutsche Bundeskanzler / die deutsche Bundeskanzlerin am häufigsten auf? Am häufigsten ist er / sie",
        "c": [
            "in Bonn, weil sich dort das Bundeskanzleramt und der Bundestag befinden.",
	    "auf Schloss Meseberg, dem Gästehaus der Bundesregierung, um Staatsgäste zu empfangen.",
	    "auf Schloss Bellevue, dem Amtssitz des Bundespräsidenten / der Bundespräsidentin, um Staatsgäste zu empfangen.",
	    "in Berlin, weil sich dort das Bundeskanzleramt und der Bundestag befinden."
        ]
    },
    "72": {
        "q": "Wie heißt der jetzige Bundeskanzler / die jetzige Bundeskanzlerin von Deutschland? (2024)",
        "c": [
            "Gerhard Schröder",
	    "Angela Merkel",
	    "Franziska Giffey",
	    "Olaf Scholz"
        ]
    },
    "73": {
        "q": "Die beiden größten Fraktionen im Deutschen Bundestag heißen zurzeit (2024)",
        "c": [
            "CDU/CSU und SPD.",
	    "Die Linke und Bündnis 90/Die Grünen.",
	    "FDP und SPD.",
	    "Die Linke und FDP."
        ]
    },
    "74": {
        "q": "Wie heißt das Parlament für ganz Deutschland?",
        "c": [
            "Bundesversammlung",
            "Volkskammer",
            "Bundestag",
            "Bundesgerichtshof"
        ]
    },
    "75": {
        "q": "Wie heißt Deutschlands heutiges Staatsoberhaupt? (2024)",
        "c": [
            "Frank-Walter Steinmeier",
	    "Bärbel Bas",
	    "Bodo Ramelow",
	    "Joachim Gauck"
        ]
    },
    "76": {
        "q": "Was bedeutet die Abkürzung CDU in Deutschland?",
        "c": [
            "Christliche Deutsche Union",
	    "Club Deutscher Unternehmer",
	    "Christlicher Deutscher Umweltschutz",
	    "Christlich Demokratische Union"
        ]
    },
    "77": {
        "q": "Was ist die Bundeswehr?",
        "c": [
            "die deutsche Polizei",
            "ein deutscher Hafen",
            "eine deutsche Bürgerinitiative",
            "die deutsche Armee",
        ]
    },
    "78": {
        "q": "Was bedeutet die Abkürzung SPD?",
        "c": [
            "Sozialistische Partei Deutschlands",
            "Sozialpolitische Partei Deutschlands",
            "Sozialdemokratische Partei Deutschlands",
            "Sozialgerechte Partei Deutschlands"
        ]
    },
    "79": {
        "q": "Was bedeutet die Abkürzung FDP in Deutschland?",
        "c": [
            "Friedliche Demonstrative Partei",
            "Freie Deutschland Partei",
            "Führende Demokratische Partei",
            "Freie Demokratische Partei"
        ]
    },
    "80": {
        "q": "Welches Gericht in Deutschland ist zuständig für die Auslegung des Grundgesetzes?",
        "c": [
            "Oberlandesgericht",
            "Amtsgericht",
            "Bundesverfassungsgericht",
            "Verwaltungsgericht"
        ]
    },
    "81": {
        "q": "Wer wählt den Bundeskanzler / die Bundeskanzlerin in Deutschland?",
        "c": [
            "der Bundesrat",
            "die Bundesversammlung",
            "das Volk",
            "der Bundestag"
        ]
    },
    "82": {
        "q": "Wer leitet das deutsche Bundeskabinett?",
        "c": [
            "der Bundestagspräsident / die Bundestagspräsidentin",
            "der Bundespräsident / die Bundespräsidentin",
            "der Bundesratspräsident / die Bundesratspräsidentin",
            "der Bundeskanzler / die Bundeskanzlerin"
        ]
    },
    "83": {
        "q": "Wer wählt den deutschen Bundeskanzler / die deutsche Bundeskanzlerin?",
        "c": [
            "das Volk",
            "die Bundesversammlung",
            "der Bundestag",
            "die Bundesregierung"
        ]
    },
    "84": {
        "q": "Welche Hauptaufgabe hat der deutsche Bundespräsident / die deutsche Bundespräsidentin? Er/Sie",
        "c": [
            "regiert das Land.",
            "entwirft die Gesetze.",
            "repräsentiert das Land.",
            "überwacht die Einhaltung der Gesetze."
        ]
    },
    "85": {
        "q": "Wer bildet den deutschen Bundesrat?",
        "c": [
            "die Abgeordneten des Bundestages",
            "die Minister und Ministerinnen der Bundesregierung",
            "die Regierungsvertreter der Bundesländer",
            "die Parteimitglieder"
        ]
    },
    "86": {
        "q": "Wer wählt in Deutschland den deutschen Bundespräsident / die Bundespräsidentin?",
        "c": [
            "die Bundesversammlung",
            "der Bundesrat",
            "das Bundesparlament",
            "das Bundesverfassungsgericht"
        ]
    },
    "87": {
        "q": "Wer ist das Staatsoberhaupt der Bundesrepublik Deutschland?",
        "c": [
            "der Bundeskanzler / die Bundeskanzlerin",
            "der Bundespräsident / die Bundespräsidentin",
            "der Bundesratspräsident / die Bundesratspräsidentin",
            "der Bundestagspräsident / die Bundestagspräsidentin"
        ]
    },
    "88": {
        "q": "Die parlamentarische Opposition im Deutschen Bundestag",
        "c": [
            "kontrolliert die Regierung.",
            "entscheidet, wer Bundesminister / Bundesministerin wird.",
            "bestimmt, wer im Bundesrat sitzt.",
            "schlägt die Regierungschefs / Regierungschefinnen der Länder vor."
        ]
    },
    "89": {
        "q": "Wie nennt man in Deutschland die Vereinigung von Abgeordneten einer Partei im Parlament?",
        "c": [
            "Verband",
            "Ältestenrat",
            "Fraktion",
            "Opposition"
        ]
    },
    "90": {
        "q": "Die deutschen Bundesländer wirken an der Gesetzgebung des Bundes mit durch",
        "c": [
            "den Bundesrat.",
            "die Bundesversammlung.",
            "den Bundestag.",
            "die Bundesregierung."
        ]
    },
    "91": {
        "q": "In Deutschland kann ein Regierungswechsel in einem Bundesland Auswirkungen auf die Bundespolitik haben. Das Regieren wird",
        "c": [
            "schwieriger, wenn sich dadurch die Mehrheit im Bundestag ändert.",
            "leichter, wenn dadurch neue Parteien in den Bundesrat kommen.",
            "schwieriger, wenn dadurch die Mehrheit im Bundesrat verändert wird.",
            "leichter, wenn es sich um ein reiches Bundesland handelt."
        ]
    },
    "92": {
        "q": "Was bedeutet die Abkürzung CSU in Deutschland?",
        "c": [
            "Christlich Sichere Union",
            "Christlich Süddeutsche Union",
            "Christlich Sozialer Unternehmerverband",
            "Christlich Soziale Union"
        ]
    },
    "93": {
        "q": "Je mehr 'Zweitstimmen' eine Partei bei einer Bundestagswahl bekommt, desto",
        "c": [
            "weniger Erststimmen kann sie haben.",
            "mehr Direktkandidaten der Partei ziehen ins Parlament ein.",
            "größer ist das Risiko, eine Koalition bilden zu müssen.",
            "mehr Sitze erhält die Partei im Parlament."
        ]
    },
    "94": {
        "q": "Ab welchem Alter darf man in Deutschland an der Wahl zum Deutschen Bundestag teilnehmen?",
        "c": [
            "16",
            "18",
            "21",
            "23"
        ]
    },
    "95": {
        "q": "Was gilt für die meisten Kinder in Deutschland?",
        "c": [
            "Wahlpflicht",
            "Schulpflicht",
            "Schweigepflicht",
            "Religionspflicht"
        ]
    },
    "96": {
        "q": "Was muss jeder deutsche Staatsbürger / jede deutsche Staatsbürgerin ab dem 16. Lebensjahr besitzen?",
        "c": [
            "einen Reisepass",
            "einen Personalausweis",
            "einen Sozialversicherungsausweis",
            "einen Führerschein"
        ]
    },
    "97": {
        "q": "Was bezahlt man in Deutschland automatisch, wenn man fest angestellt ist?",
        "c": [
            "Sozialversicherung",
            "Sozialhilfe",
            "Kindergeld",
            "Wohngeld"
        ]
    },
    "98": {
        "q": "Wenn Abgeordenete im Deutschen Bundestag ihre Fraktion wechseln, ",
        "c": [
            "dürfen sie nicht mehr an den Sitzungen des Parlaments teilnehmen.",
            "kann die Regierung ihre Mehrheit verlieren.",
            "muss der Bundespräsident / die Bundespräsidentin zuvor sein / ihr Einverständnis geben.",
            "dürfen die Wähler / Wählerinnen dieser Abgeordneten noch einmal wählen."
        ]
    },
    "99": {
        "q": "Wer bezahlt in Deutschland die Sozialversicherungen?",
        "c": [
            "Arbeitgeber / Arbeitgeberinnen und Arbeitnehmer / Arbeitnehmerinnen",
            "nur Arbeitnehmer / Arbeitnehmerinnen",
            "alle Staatsangehörigen",
            "nur Arbeitgeber / Arbeitgeberinnen"
        ]
    },
    "100": {
        "q": "Was gehört nicht zur gesetzlichen Sozialversicherung?",
        "c": [
            "die Lebensversicherung",
            "die gesetzliche Rentenversicherung",
            "die Arbeitslosenversicherung",
            "die Pflegeversicherung"
        ]
    },
    "101": {
        "q": "Gewerkschaften sind Interessenverbände der",
        "c": [
            "Jugendlichen.",
            "Arbeitnehmer und Arbeitnehmerinnen.",
            "Rentner und Rentnerinnen.",
            "Arbeitgeber und Arbeitgeberinnen."
        ]
    },
    "102": {
        "q": "Womit kann man in der Bundesrepublik Deutschland geehrt werden, wenn man auf politischen, wirtschaftlichem, kulturellem, geistigem oder sozialem Gebiet eine besondere Leistung erbracht hat? Mit dem",
        "c": [
            "Bundesverdienstkreuz",
            "Bundesadler",
            "Vaterländischen Verdienstorden",
            "Ehrentitel 'Held der Deutschen Demokratischen Republik'"
        ]
    },
    "103": {
        "q": "Was wird in Deutschland als 'Ampelkoalition' bezeichnet? Die Zusammenarbeit",
        "c": [
            "der Bundestagsfraktionen von CDU und CSU",
            "von SPD, FDP und Bündnis 90/Die Grünen in einer Regierung",
            "von CSU, Die LINKE und Bündnis 90/Die Grünen in einer Regierung",
            "der Bundestagsfraktionen von CDU und SPD"
        ]
    },
    "104": {
        "q": "Eine Frau in Deutschland verliert ihre Arbeit. Was darf nicht der Grund für diese Entlassung sein?",
        "c": [
            "Die Frau ist lange krank und arbeitsunfähig.",
            "Die Frau kam oft zu spät zur Arbeit.",
            "Die Frau erledigt private Sachen während der Arbeitszeit.",
            "Die Frau bekommt ein Kind und ihr Chef weiß das."
        ]
    },
    "105": {
        "q": "Was ist eine Aufgabe von Wahlhelfern / Wahlhelferinnen in Deutschland?",
        "c": [
            "Sie helfen alten Menschen bei der Stimmabgabe in der Wahlkabine.",
            "Sie schreiben die Wahlbenachrichtigungen vor der Wahl.",
            "Sie geben Zwischenergebnisse an die Medien weiter.",
            "Sie zählen die Stimmen nach dem Ende der Wahl."
        ]
    },
    "106": {
        "q": "In Deutschland helgen ehrenamtliche Wahlhelfer und Wahlhelferinnen bei den Wahlen. Was ist eine Aufgabe von Wahlhelfern / Wahlhelferinnen?",
        "c": [
            "Sie helfen Kindern und alten Menschen beim Wählen.",
            "Sie schreiben Karten und Briefe mit der Angabe des Wahllokals.",
            "Sie geben Zwischenergebnisse an Journalisten weiter.",
            "Sie zählen die Stimmen nach dem Ende der Wahl."
        ]
    },
    "107": {
        "q": "Für wie viele Jahre wird der Bundestag in Deutschland gewählt?",
        "c": [
            "2 Jahre",
            "4 Jahre",
            "6 Jahre",
            "8 Jahre"
        ]
    },
    "108": {
        "q": "Bei einer Bundestagswahl in Deutschland darf jeder wählen, der",
        "c": [
            "in der Bundesrepublik Deutschland wohnt und wählen möchte.",
            "Bürger / Bürgerin der Bundesrepublik Deutschland ist und mindestens 18 Jahre alt ist.",
            "seit mindestens 3 Jahren in der Bundesrepublik Deutschland lebt.",
            "Bürger / Bürgerin der Bundesrepublik Deutschland ist und mindestens 21 Jahre alt ist."
        ]
    },
    "109": {
        "q": "Wie oft gibt es normalerweise Bundestagswahlen in Deutschland?",
        "c": [
            "alle drei Jahre",
            "alle vier Jahre",
            "alle fünf Jahre",
            "alle sechs Jahre"
        ]
    },
    "110": {
        "q": "Für wie viele Jahre wird der Bundestag in Deutschland gewählt?",
        "c": [
            "2 Jahre",
            "3 Jahre",
            "4 Jahre",
            "5 Jahre"
        ]
    },
    "111": {
        "q": "In Deutschland darf man wählen. Was bedeutet das?",
        "c": [
            "Alle deutschen Staatsangehörigen dürfen wählen, wenn sie das Mindestalter erreicht haben.",
            "Nur verheiratete Personen dürfen wählen.",
            "Nur Personen mit einem festen Arbeitsplatz dürfen wählen.",
            "Alle Einwohner und Einwohnerinnen in Deutschland müssen wählen."
        ]
    },
    "112": {
        "q": "Die Wahlen in Deutschland sind",
        "c": [
            "speziell.",
            "geheim.",
            "berufsbezogen.",
            "geschlechtsabhängig."
        ]
    },
    "113": {
        "q": "Wahlen in Deutschland gewinnt die Partei, die",
        "c": [
            "die meisten Stimmen bekommt.",
            "die meisten Männer mehrheitlich gewählt haben.",
            "die meisten Stimmen bei den Arbeitern / Arbeiterinnen bekommen hat.",
            "die meisten Erststimmen für ihren Kanzlerkandidaten / ihre Kanzlerkandidatin erhalten hat."
        ]
    },
    "114": {
        "q": "An demokratischen Wahlen in Deutschland teilzunehmen ist",
        "c": [
            "eine Pflicht.",
            "ein Recht.",
            "ein Zwang.",
            "eine Last."
        ]
    },
    "115": {
        "q": "Was bedeutet 'aktives Wahlrecht' in Deutschland?",
        "c": [
            "Man kann gewählt werden.",
            "Man muss wählen gehen.",
            "Man kann wählen.",
            "Man muss zur Auszählung der Stimmen gehen."
        ]
    },
    "116": {
        "q": "Wenn Sie bei einer Bundestagswahl in Deutschland wählen dürfen, heißt das",
        "c": [
            "aktive Wahlkampagne.",
            "aktives Wahlverfahren.",
            "aktiver Wahlkampf.",
            "aktives Wahlrecht."
        ]
    },
    "117": {
        "q": "Wie viel Prozent der Zweitstimmen müssen Parteien mindestens bekommen, um in den Deutschen Bundestag gewählt zu werden?",
        "c": [
            "3%",
            "4%",
            "5%",
            "6%"
        ]
    },
    "118": {
        "q": "Was regelt das Wahlrecht in Deutschland?",
        "c": [
            "Wer wählen darf, muss wählen.",
            "Alle die wollen, können wählen.",
            "Wer nicht wählt, verliert das Recht zu wählen.",
            "Wer wählen darf, kann wählen."
        ]
    },
    "119": {
        "q": "Wahlen in Deutschland sind frei. Was bedeutet das?",
        "c": [
            "Alle verurteilten Straftäter / Straftäterinnen dürfen nicht wählen.",
            "Wenn ich wählen gehen möchte, muss mein Arbeitgeber / meine Arbeitgeberin mir frei geben.",
            "Jede Person kann ohne Zwang entscheiden, ob sie wählen möchte und wen sie wählen möchte.",
            "Ich kann frei entscheiden, wo ich wählen gehen möchte."
        ]
    },
    "120": {
        "q": "Das Wahlsystem in Deutschland ist ein",
        "c": [
            "Zensuswahlrecht.",
            "Dreiklassenwahlrecht.",
            "Mehrheits- und Verhältniswahlrecht.",
            "allgemeines Männerwahlrecht."
        ]
    },
    "121": {
        "q": "Eine Partei möchte in den Deutschen Bundestag. Sie muss aber einen Mindestanteil an Wählerstimmen haben. Das heißt",
        "c": [
            "5%-Hürde.",
            "Zulassungsgrenze.",
            "Basiswert.",
            "Richtlinie."
        ]
    },
    "122": {
        "q": "Welchem Grundsatz unterliegen Wahlen in Deutschland? Wahlen in Deutschland sind",
        "c": [
            "frei, gleich, geheim.",
            "offen, sicher, frei.",
            "geschlossen, gleich, sicher.",
            "sicher, offen, freiwillig."
        ]
    },
    "123": {
        "q": "Was ist in Deutschland die '5%-Hürde'?",
        "c": [
            "Abstimmungsregelung im Bundestag für kleine Parteien",
            "Anwesenheitskontrolle im Bundestag für Abstimmungen",
            "Mindestanteil an Wählerstimmen, um ins Parlament zu kommen",
            "Anwesenheitskontrolle im Bundesrat für Abstimmungen"
        ]
    },
    "124": {
        "q": "Die Bundestagswahl in Deutschland ist die Wahl",
        "c": [
            "des Bundeskanzlers / der Bundeskanzlerin.",
            "der Parlamente der Länder.",
            "des Parlaments für Deutschland.",
            "des Bundespräsidenten / der Bundespräsidentin."
        ]
    },
    "125": {
        "q": "In einer Demokratie ist eine Funktion von regelmäßigen Wahlen",
        "c": [
            "die Bürger und Bürgerinnen zu zwingen, ihre Stimme abzugeben.",
            "nach dem Willen der Wählermehrheit den Wechsel der Regierung zu ermöglichen.",
            "im Land bestehende Gesetze beizubehalten.",
            "den Armen mehr Macht zu geben."
        ]
    },
    "126": {
        "q": "Was bekommen wahlberechtigte Bürger und Bürgerinnen in Deutschland vor einer Wahl?",
        "c": [
            "eine Wahlbenachrichtigung von der Gemeinde",
            "eine Wahlerlaubnis vom Bundespräsidenten / von der Bundespräsidentin",
            "eine Benachrichtigung von der Bundesversammlung",
            "eine Benachrichtigung vom Pfarramt"
        ]
    },
    "127": {
        "q": "Warum gibt es die 5%-Hürde im Wahlgesetz der Bundesrepublik Deutschland? Es gibt sie, weil",
        "c": [
            "die Programme von vielen kleinen Parteien viele Gemeinsamkeiten haben.",
            "die Bürger und Bürgerinnen bei vielen kleinen Parteien die Orientierung verlieren können.",
            "viele kleine Parteien die Regierungsbildung erschweren.",
            "die kleinen Parteien nicht so viel Geld haben, um die Politiker und Politikerinnen zu bezahlen."
        ]
    },
    "128": {
        "q": "Parlamentsmitglieder, die von den Bürger und Bürgerinnen gewählt werden, nennt man",
        "c": [
            "Abgeordnete.",
            "Kanzler / Kanzlerinnen.",
            "Botschafter / Botschafterinnen.",
            "Ministerpräsidenten / Ministerpräsidentinnen."
        ]
    },
    "129": {
        "q": "Vom Volk gewählt wird in Deutschland",
        "c": [
            "der Bundeskanzler / die Bundeskanzlerin.",
            "der Ministerpräsident / die Ministerpräsidentin eines Bundeslandes.",
            "der Bundestag.",
            "der Bundespräsident / die Bundespräsidentin."
        ]
    },
    "130": {
        "q": "Welcher Stimmzettel wäre bei einer Bundestagswahl gültig? <br><img src='./images/vote-letter.png'>",
        "c": [
            "1",
            "2",
            "3",
            "4"
        ]
    },
    "131": {
        "q": "In Deutschland ist ein Bürgermeister / eine Bürgermeisterin",
        "c": [
            "der Leiter / die Leiterin einer Schule.",
            "der Chef / die Chefin einer Bank.",
                "das Oberhaupt einer Gemeinde.",
            "der / die Vorsitzende einer Partei. "
        ]
    },
    "132": {
        "q": "Viele Menschen in Deutschland arbeiten in ihrer Freizeit ehrenamtlich. Was bedeutet das?",
        "c": [
            "Sie arbeiten als Soldaten / Soldatinnen.",
            "Sie arbeiten freiwillig und unbezahlt in Vereinen und Verbänden.",
            "Sie arbeiten in der Bundesregierung.",
            "Sie arbeiten in einem Krankenhaus und verdienen dabei Geld."
        ]
    },
    "133": {
        "q": "Was ist bei Bundestags- und Landtagswahlen in Deutschland erlaubt?",
        "c": [
            "Der Ehemann wählt für seine Frau mit.",
            "Man kann durch Briefwahl seine Stimme abgeben.",
            "Man kann am Wahltag telefonisch seine Stimme abgeben.",
            "Kinder ab dem Alter von 14 Jahren dürfen wählen."
        ]
    },
    "134": {
        "q": "Man will die Buslinie abschaffen, mit der Sie immer zu Arbeit fahren. Was können Sie machen, um die Buslinie zu erhalten?",
        "c": [
            "Ich beteilige mich an einer Bürgerinitiative für die Erhaltung der Buslinie oder gründe selber eine Initiative.",
            "Ich werde Mitglied in einem Sportverein und trainiere Radfahren.",
            "Ich wende mich an das Finanzamt, weil ich als Steuerzahler / Steuerzahlerin ein Recht auf die Buslinie habe.",
            "Ich schreibe einen Brief an das Forstamt der Gemeinde."
        ]
    },
    "135": {
        "q": "Wen vertreten die Gewerkschaften in Deutschland?",
        "c": [
            "große Unternehmen",
            "kleine Unternehmen",
            "Selbständige",
            "Arbeitnehmer und Arbeitnehmerinnen"
        ]
    },
    "136": {
        "q": "Sie gehen in Deutschland zum Arbeitsgericht bei",
        "c": [
            "falscher Nebenkostenabrechnung.",
            "ungerechtfertigter Kündigung durch Ihren Chef / Ihre Chefin.",
            "Problemen mit den Nachbarn / Nachbarinnen.",
            "Schwierigkeiten nach einem Verkehrsunfall."
        ]
    },
    "137": {
        "q": "Welches Gericht ist in Deutschland bei Konflikten in der Arbeitswelt zuständig?",
        "c": [
            "das Familiengericht",
            "das Strafgericht",
            "das Arbeitsgericht",
            "das Amtsgericht"
        ]
    },
    "138": {
        "q": "Was kann in Deutschland machen, wenn mir mein Arbeitgeber / meine Arbeitgeberin zu Unrecht gekündigt hat?",
        "c": [
            "weiter arbeiten und freundlich zum Chef / zur Chefin sein",
            "ein Mahnverfahren gegen den Arbeitgeber / die Arbeitgeberin führen",
            "Kündigungsschutzklage erheben",
            "den Arbeitgeber / die Arbeitgeberin bei der Polizei anzeigen"
        ]
    },
    "139": {
        "q": "Wann kommt es in Deutschland zu einem Prozess vor Gericht? Wenn jemand",
        "c": [
            "zu einer anderen Religion übertritt.",
            "eine Straftat begangen hat und angeklagt wird.",
            "eine andere Meinung als die der Regierung vertritt.",
            "sein Auto falsch geparkt hat und es abgeschleppt wird."
        ]
    },
    "140": {
        "q": "Was macht ein Schöffe / eine Schöffin in Deutschland? Er / Sie",
        "c": [
            "entscheidet mit Richtern / Richterinnen über Schuld und Strafe.",
            "gibt Bürgern / Bürgerinnen rechtlichen Rat.",
            "stellt Urkunden aus.",
            "verteidigt den Angeklagten / die Angeklagte."
        ]
    },
    "141": {
        "q": "Wer berät in Deutschland Personen bei Rechtsfragen und vertritt sie vor Gericht?",
        "c": [
            "ein Rechtsanwalt / eine Rechtsanwältin",
            "ein Richter / eine Richterin",
            "ein Schöffe / eine Schöffin",
            "ein Staatsanwalt / eine Staatsanwältin"
        ]
    },
    "142": {
        "q": "Was ist die Hauptaufgabe eines Richters / einer Richterin in Deutschland? Ein Richter / eine Richterin",
        "c": [
            "vertritt Bürger und Bürgerinnen vor einem Gericht.",
            "arbeitet an einem Gericht und spricht Urteile.",
            "ändert Gesetze.",
            "betreut Jugendliche vor Gericht."
        ]
    },
    "143": {
        "q": "Ein Richter / eine Richterin in Deutschland gehört zur",
        "c": [
            "Judikative",
            "Exekutive",
            "Operative",
            "Legislative"
        ]
    },
    "144": {
        "q": "Ein Richter / eine Richterin gehört in Deutschland zur",
        "c": [
            "vollziehenden Gewalt.",
            "rechtsprechenden Gewalt.",
            "planenden Gewalt.",
            "gesetzgebenden Gewalt."
        ]
    },
    "145": {
        "q": "In Deutschland wird die Staatsgewalt geteilt. Für welche Staatsgewalt arbeitet ein Richter / einer Richterin? Für die",
        "c": [
            "Judikative",
            "Exekutive",
            "Presse",
            "Legislative"
        ]
    },
    "146": {
        "q": "Wie nennt man in Deutschland ein Verfahren vor einem Gericht?",
        "c": [
            "Programm",
            "Prozedur",
            "Protokoll",
            "Prozess"
        ]
    },
    "147": {
        "q": "Was ist die Arbeit eines Richters / einer Richterin Deutschland?",
        "c": [
            "Deutschland regieren",
            "Recht sprechen",
            "Pläne erstellen",
            "Gesetze erlassen"
        ]
    },
    "148": {
        "q": "Was ist eine Aufgabe der Polizei in Deutschland?",
        "c": [
            "das Land zu verteidigen",
            "die Bürgerinnen und Bürger abzuhören",
            "die Gesetze zu beschließen",
            "die Einhaltung von Gesetzen zu überwachen"
        ]
    },
    "149": {
        "q": "Wer kann Gerichtsschöffe / Gerichtsschöffin in Deutschland werden?",
        "c": [
            "alle in Deutschland geborenen Einwohner / Einwohnerinnen über 18 Jahre",
            "alle deutschen Staatsangehörigen älter als 24 und jünger als 70 Jahre",
            "alle Personen, die seit mindestens 5 Jahren in Deutschland leben",
            "nur Personen mit einem abgeschlossenen Jurastudium"
        ]
    },
    "150": {
        "q": "Ein Gerichtsschöffe / eine Gerichtsschöffin in Deutschland ist",
        "c": [
            "der Stellvertreter / die Stellvertreterin des Stadtoberhaupts.",
            "ein ehrenamtlicher Richter / eine ehrenamtliche Richterin.",
            "ein Mitglied eines Gemeinderats.",
            "eine Person, die Jura studiert hat."
        ]
    },
    "151": {
        "q": "Wer baute die Mauer in Berlin?",
        "c": [
            "Großbritannien",
            "die DDR",
            "die Bundesrepublik Deutschland",
            "die USA"
        ]
    },
    "152": {
        "q": "Wann waren die Nationalsozialisten mit Adolf Hitler in Deutschland an der Macht?",
        "c": [
            "1918 bis 1923",
            "1932 bis 1950",
            "1933 bis 1945",
            "1945 bis 1989"
        ]
    },
    "153": {
        "q": "Was war am 8. Mai 1945?",
        "c": [
            "Tod Adolf Hitlers",
            "Beginn des Berliner Mauerbaus",
            "Wahl von Konrad Adenauer zum Bundeskanzler",
            "Ende des Zweiten Weltkriegs in Europa"
        ]
    },
    "154": {
        "q": "Wann war der Zweite Weltkrieg zu Ende?",
        "c": [
            "1933",
            "1945",
            "1949",
            "1961"
        ]
    },
    "155": {
        "q": "Wann waren die Nationalsozialisten in Deutschland an der Macht?",
        "c": [
            "1888 bis 1918",
            "1921 bis 1934",
            "1933 bis 1945",
            "1949 bis 1963 "
        ]
    },
    "156": {
        "q": "In welchem Jahr wurde Hitler Reichskanzler?",
        "c": [
            "1923",
            "1927",
            "1933",
            "1936"
        ]
    },
    "157": {
        "q": "Die Nationalsozialisten mit Adolf Hitler errichteten 1933 in Deutschland",
        "c": [
            "eine Diktatur.",
            "einen demokratischen Staat.",
            "eine Monarchie.",
            "ein Fürstentum."
        ]
    },
    "158": {
        "q": "Das 'Dritte Reich' war eine",
        "c": [
            "Diktatur.",
            "Demokratie.",
            "Monarchie.",
            "Räterepublik."
        ]
    },
    "159": {
        "q": "Was gab es in Deutschland nicht während der Zeit des Nationalsozialismus?",
        "c": [
            "freie Wahlen",
            "Pressezensur",
            "willkürliche Verhaftungen",
            "Verfolgung der Juden"
        ]
    },
    "160": {
        "q": "Welcher Krieg dauerte von 1939 bis 1945?",
        "c": [
            "der Erste Weltkrieg",
            "der Zweite Weltkrieg",
            "der Vietnamkrieg",
            "der Golfkrieg"
        ]
    },
    "161": {
        "q": "Was kennzeichnete den NS-Staat? Eine Politik",
        "c": [
            "des staatlichen Rassismus",
            "der Meinungsfreiheit",
            "der allgemeinen Religionsfreiheit",
            "der Entwicklung der Demokratie"
        ]
    },
    "162": {
        "q": "Claus Schenk Graf von Stauffenberg wurde bekannt durch",
        "c": [
            "eine Goldmedaille bei den Olympischen Spielen 1936.",
            "den Bau des Reichstagsgebäudes.",
            "den Aufbau der Wehrmacht.",
            "das Attentat auf Hitler am 20. Juli 1944."
        ]
    },
    "163": {
        "q": "In welchem Jahr zerstörten die Nationalsozialisten Synagogen und jüdische Geschäfte in Deutschland?",
        "c": [
            "1925",
            "1930",
            "1938",
            "1945",
        ]
    },
    "164": {
        "q": "Was passierte am 9. November 1938 in Deutschland?",
        "c": [
            "Mit dem Angriff auf Polen beginnt der Zweite Weltkrieg.",
            "Die Nationalsozialisten verlieren eine Wahl und lösen den Reichstag auf.",
            "Jüdische Geschäfte und Synagogen werden durch Nationalsozialisten und ihre Anhänger zerstört.",
            "Hitler wird Reichspräsident und lässt alle Parteien verbieten."
        ]
    },
    "165": {
        "q": "Wie heiß der erste Bundeskanzler der Bundesrepublik Deutschland?",
        "c": [
            "Konrad Adenauer",
            "Kurt Georg Kiesinger",
            "Helmut Schmidt",
            "Willy Brandt"
        ]
    },
    "166": {
        "q": "Bei welchen Demonstrationen in Deutschland riefen die Menschen 'Wir sind das Volk'?",
        "c": [
            "beim Arbeiteraufstand 1953 in der DDR",
            "bei den Demonstrationen 1968 in der Bundesrepublik Deutschland",
            "bei den Anti-Atomkraft-Demonstrationen 1985 in der Bundesrepublik Deutschland",
            "bei den Montagsdemonstrationen 1989 in der DDR"
        ]
    },
    "167": {
        "q": "Welche Länder wurden nach dem Zweiten Weltkrieg in Deutschland als 'Alliierte Besatzungsmächte' bezeichnet?",
        "c": [
            "Sowjetunion, Großbritannien, Polen, Schweden",
            "Frankreich, Sowjetunion, Italien, Japan",
            "USA, Sowjetunion, Spanien, Portugal",
            "USA, Sowjetunion, Großbritannien, Frankreich"
        ]
    },
    "168": {
        "q": "Welches Land war keine 'Alliierte Besatzungsmächt' in Deutschland?",
        "c": [
            "USA",
            "Sowjetunion",
            "Frankreich",
            "Japan",
        ]
    },
    "169": {
        "q": "Wann wurde die Bundesrepublik Deutschland gegründet?",
        "c": [
            "1939",
            "1945",
            "1949",
            "1951"
        ]
    },
    "170": {
        "q": "Was gab es während der Zeit des Nationalsozialismus in Deutschland?",
        "c": [
            "das Verbot von Parteien",
            "das Recht zur freien Entfaltung der Persönlichkeit",
            "Pressefreiheit",
            "den Schutz der Menschenwürde"
        ]
    },
    "171": {
        "q": "Soziale Marktwirtschaft bedeutet, die Wirtschaft",
        "c": [
            "steuert sich allein nach Angebot und Nachfrage.",
            "wird vom Staat geplant und gesteuert, Angebot und Nachfrage werden nicht berücksichtigt.",
            "richtet sich nach der Nachfrage im Ausland.",
            "richtet sich nach Angebot und Nachfrage, aber der Staat sorgt für einen sozialen Ausgleich."
        ]
    },
    "172": {
        "q": "In welcher Besatzungszone wurde die DDR gegründet? In der",
        "c": [
            "amerikanischen Besatzungszone",
            "ranzösischen Besatzungszone",
            "britischen Besatzungszone",
            "sowjetischen Besatzungszone"
        ]
    },
    "173": {
        "q": "Die Bundesrepublik Deutschland ist ein Gründungsmitglied",
        "c": [
            "des Nordatlantikpakts (NATO).",
            "der Vereinten Nationen (VN).",
            "der Europäischen Union (EU).",
            "des Warschauer Pakts."
        ]
    },
    "174": {
        "q": "Wann wurde die DDR gegründet?",
        "c": [
            "1947",
            "1949",
            "1953",
            "1956"
        ]
    },
    "175": {
        "q": "Wie viele Besatzungszonen gab es in Deutschland nach dem Zweiten Weltkrieg?",
        "c": [
            "3",
            "4",
            "5",
            "6"
        ]
    },
    "176": {
        "q": "Wie waren die Besatzungszonen Deutschlands nach 1945 verteilt? <br><img src='./images/occupation-zones.png'>",
        "c": [
            "1=Großbritannien, 2=Sowjetunion, 3=Frankreich, 4=USA",
            "1=Sowjetunion, 2=Großbritannien, 3=USA, 4=Frankreich",
            "1=Großbritannien, 2=Sowjetunion, 3=USA, 4=Frankreich",
            "1=Großbritannien, 2=USA, 3=Sowjetunion, 4=Frankreich"
        ]
    },
    "177": {
        "q": "Welche deutsche Stadt wurde nach dem Zweiten Weltkrieg in vier Sektoren aufgeteilt?",
        "c": [
            "München",
            "Berlin",
            "Dresden",
            "Frankfurt/Oder"
        ]
    },
    "178": {
        "q": "Vom Juni 1948 bis zum Mai 1949 wurden die Bürger und Bürgerinnen von West-Berlin durch eine LuftBrücke versorgt. Welcher Umstand war dafür verantwortlich?",
        "c": [
            "Für Frankreich war eine Versorgung der West-Berliner Bevölkerung mit dem Flugzeug kostengünstiger.",
            "Die amerikanischen Soldaten / Soldatinnen hatten beim Landtransport Angst vor Überfällen.",
            "Für Großbritannien war die Versorgung über die Luftbrücke schneller.",
            "Die Sowjetunion unterbrach den gesamten Verkehr auf dem Landwege."
        ]
    },
    "179": {
        "q": "Wie endete der Zweite Weltkrieg in Europa offiziell?",
        "c": [
            "mit dem Tod Adolf Hitlers",
            "durch die bedingungslose Kapitulation Deutschlands",
            "mit dem Rückzug der Deutschen aus den besetzten Gebieten",
            "durch eine Revolution in Deutschland"
        ]
    },
    "180": {
        "q": "Der erste Bundeskanzler der Bundesrepublik Deutschland war",
        "c": [
            "Ludwig Erhard.",
            "Willy Brandt.",
            "Konrad Adenauer.",
            "Gerhard Schröder."
        ]
    },
    "181": {
        "q": "Was wollte Willy Brandt mit seinem Kniefall 1970 im ehemaligen jüdischen Ghetto in Warschau ausdrücken? <br><img src='./images/willy-brandt.png'>",
        "c": [
            "Er hat sich den ehemaligen Alliierten unterworfen.",
            "Er bat Polen und die polnischen Juden um Vergebung.",
            "Er zeigte seine Demut vor dem Warschauer Pakt.",
            "Er sprach ein Gebet am Grab des Unbekannten Soldaten."
        ]
    },
    "182": {
        "q": "Welche Parteien wurden 1946 zwangsweise zur SED vereint, der Einheitspartei der spätern DDR?",
        "c": [
            "KPD und SPD",
            "SPD und CDU",
            "CDU und FDP",
            "KPD und CSU"
        ]
    },
    "183": {
        "q": "Wann war in der Bundesrepublik Deutschland das 'Wirtschaftswunder'?",
        "c": [
            "40er Jahre",
            "50er Jahre",
            "70er Jahre",
            "80er Jahre"
        ]
    },
    "184": {
        "q": "Was nannten die Menschen in Deutschland sehr lange 'Die Stunde Null'?",
        "c": [
            "Damit wird die Zeit nach der Wende im Jahr 1989 bezeichnet.",
            "Damit wurde der Beginn des Zweiten Weltkrieges bezeichnet.",
            "Darunter verstand man das Ende des Zweiten Weltkrieges und den Beginn des Wiederaufbaus.",
            "Damit ist die Stunde gemeint, in der die Uhr von der Sommerzeit auf die Winterzeit umgestellt wird."
        ]
    },
    "185": {
        "q": "Wofür stand der Ausdruck 'Eiserner Vorhang'? Für die Abschottung",
        "c": [
            "des Warschauer Pakts gegen den Westen",
            "Norddeutschlands gegen Süddeutschland",
            "Nazi-Deutschlands gegen die Alliierten",
            "Europas gegen die USA"
        ]
    },
    "186": {
        "q": "Im Jahr 1953 gab es in der DDR einen Aufstand, an den lange Zeit in der Bundesrepublik Deutschland ein Feiertag erinnerte. Wann war das?",
        "c": [
            "1. Mai",
            "17. Juni",
            "20. Juli",
            "9. November"
        ]
    },
    "187": {
        "q": "Welcher deutsche Staat hatte eine schwarz-rot-goldene Flagge mit Hammer, Zirkel und Ährenkranz? <br><img src='./images/state-flag.png'>",
        "c": [
            "Preußen",
            "Bundesrepublik Deutschland",
            "'Drittes Reich'",
            "DDR"
        ]
    },
    "188": {
        "q": "In welchem Jahr wurde die Mauer in Berlin gebaut?",
        "c": [
            "1953",
            "1956",
            "1959",
            "1961"
        ]
    },
    "189": {
        "q": "Wann baute die DDR die Mauer in Berlin?",
        "c": [
            "1919",
            "1933",
            "1961",
            "1990"
        ]
    },
    "190": {
        "q": "Was bedeutet die Abkürzung DDR?",
        "c": [
            "Dritter Deutscher Rundfunk",
            "Die Deutsche Republik",
            "Dritte Deutsche Republik",
            "Deutsche Demokratische Republik"
        ]
    },
    "191": {
        "q": "Wann wurde die Mauer in Berlin für alle geöffnet?",
        "c": [
            "1987",
            "1989",
            "1992",
            "1995"
        ]
    },
    "192": {
        "q": "Welches heutige deutsche Bundesland gehörte früher zum Gebiet der DDR?",
        "c": [
            "Brandenburg",
            "Bayern",
            "Saarland",
            "Hessen"
        ]
    },
    "193": {
        "q": "Von 1961 bis 1989 war Berlin",
        "c": [
            "ohne Bürgermeister.",
            "ein eigener Staat.",
            "durch eine Mauer geteilt.",
            "nur mit dem Flugzeug erreichbar."
        ]
    },
    "194": {
        "q": "Am 3. Oktober feiert man in Deutschland den Tag der Deutschen",
        "c": [
            "Einheit.",
            "Nation.",
            "Bundesländer.",
            "Städte."
        ]
    },
    "195": {
        "q": "Welches heutige deutsche Bundesland gehörte früher zum Gebiet der DDR?",
        "c": [
            "Hessen",
            "Sachsen-Anhalt",
            "Nordrhein-Westfalen",
            "Saarland"
        ]
    },
    "196": {
        "q": "Warum nennt man die Zeit im Herbst 1989 in der DDR 'Die Wende'? In dieser Zeit veraenderte sich die DDR politisch",
        "c": [
            "von einer Diktatur zur Demokratie.",
            "von einer liberalen Marktwirtschaft zum Sozialismus.",
            "von einer Monarchie zur Sozialdemokratie.",
            "von einem religiösen Staat zu einem kommunistischen Staat."
        ]
    },
    "197": {
        "q": "Welches heutige deutsche Bundesland gehörte früher zum Gebiet der DDR?",
        "c": [
            "Thüringen",
            "Hessen",
            "Bayern",
            "Bremen"
        ]
    },
    "198": {
        "q": "Welches heutige deutsche Bundesland gehörte früher zum Gebiet der DDR?",
        "c": [
            "Bayern",
            "Niedersachsen",
            "Sachsen",
            "Baden-Württemberg"
        ]
    },
    "199": {
        "q": "Mit der Abkürzung 'Stasi' meinte mann in der DDR",
        "c": [
            "das Parlament.",
            "das Ministerium für Staatssicherheit.",
            "eine regierende Partei.",
            "das Ministerium für Volksbildung. "
        ]
    },
    "200": {
        "q": "Welches heutige deutsche Bundesland gehörte früher zum Gebiet der DDR?",
        "c": [
            "Hessen",
            "Schleswig-Holstein",
            "Mecklenburg-Vorpommern",
            "Saarland"
        ]
    },
    "201": {
        "q": "Welche der folgenden Auflistungen enthält nur Bundesländer, die zum Gebiet der früheren DDR gehörten?",
        "c": [
            "Niedersachsen, Nordrhein-Westfalen, Hessen, Schleswig-Holstein, Brandenburg",
            "Mecklenburg-Vorpommern, Brandenburg, Sachsen, Sachsen-Anhalt, Thüringen",
            "Bayern, Baden-Württemberg, Rheinland-Pfalz, Thüringen, Sachsen",
            "Sachsen, Thüringen, Hessen, Niedersachen, Brandenburg"
        ]
    },
    "202": {
        "q": "Zu wem gehörte die DDR im 'Kalten Krieg'?",
        "c": [
            "zu den Westmächten",
            "zum Warschauer Pakt",
            "zur NATO",
            "zu den blockfreien Staaten"
        ]
    },
    "203": {
        "q": "Wie heiß das Wirtschaftssystem der DDR?",
        "c": [
            "Marktwirtschaft",
            "Planwirtschaft",
            "Angebot und Nachfrage",
            "Kapitalismus"
        ]
    },
    "204": {
        "q": "Wie wurden die Bundesrepublik Deutschland und die DDR zu einem Staat?",
        "c": [
            "Die Bundesrepublik hat die DDR besetzt.",
            "Die heutigen fünf östlichen Bundesländer sind der Bundesrepublik Deutschland beigetreten.",
            "Die westlichen Bundesländer sind der DDR beigetreten.",
            "Die DDR hat die Bundesrepublik Deutschland besetzt."
        ]
    },
    "205": {
        "q": "Mit dem Beitritt der DDR zur Bundesrepublik Deutschland gehören die neuen Bundesländer nun auch",
        "c": [
            "zur Europäischen Union.",
            "zum Warschauer Pakt.",
            "zur OPEC.",
            "zur Europäischen Verteidigungsgemeinschaft."
        ]
    },
    "206": {
        "q": "Was bedeutete im Jahr 1989 in Deutschland das Wort 'Montagsdemonstration'?",
        "c": [
            "In der Bundesrepublik waren Demonstrationen nur am Montag erlaubt.",
            "Montags waren Demonstrationen gegen das DDR-Regime.",
            "Am ersten Montag im Monat trafen sich in der Bundesrepublik Deutschland Demonstranten.",
            "Montags demonstrierte man in der DDR gegen den Westen."
        ]
    },
    "207": {
        "q": "In welchem Militärbündnis war die DDR Mitglied?",
        "c": [
            "in der NATO",
            "im Rheinbund",
            "im Warschauer Pakt",
            "im Europabündnis"
        ]
    },
    "208": {
        "q": "Was war die 'Stasi'?",
        "c": [
            "der Geheimdienst im 'Dritten Reich'",
            "eine berühmte deutsche Gedenkstätte",
            "der Geheimdienst der DDR",
            "ein deutscher Sportverein während des Zweiten Weltkrieges"
        ]
    },
    "209": {
        "q": "Welches war das Wappen der Deutschen Demokratischen Republik?  <br><img src='./images/ddr-flag.png'>",
        "c": [
            "1",
            "2",
            "3",
            "4"
        ]
    },
    "210": {
        "q": "War ereignete sich am 17 Juni 1953 in der DDR?",
        "c": [
            "der feierliche Beitritt zum Warschauer Pakt",
            "landesweite Streiks und ein Volksaufstand",
            "der 1. SED-Parteitag",
            "der erste Besuch Fidel Castros"
        ]
    },
    "211": {
        "q": "Welcher Politiker steht für die 'Ostverträge'?",
        "c": [
            "Helmut Kohl",
            "Willy Brandt",
            "Michail Gorbatschow",
            "Ludwig Erhard"
        ]
    },
    "212": {
        "q": "Wie heißt Deutschland mit vollem Namen?",
        "c": [
            "Bundesstaat Deutschland",
            "Bundesländer Deutschland",
            "Bundesrepublik Deutschland",
            "Bundesbezirk Deutschland"
        ]
    },
    "213": {
        "q": "Wie viele Einwohner hat Deutschland?",
        "c": [
            "70 Millionen",
            "78 Millionen",
            "83 Millionen",
            "90 Millionen"
        ]
    },
    "214": {
        "q": "Welche Farben hat die deutsche Flagge?",
        "c": [
            "schwarz-rot-gold",
            "rot-weiß-schwarz",
            "schwarz-rot-grün",
            "schwarz-gelb-rot"
        ]
    },
    "215": {
        "q": "Wer wird als 'Kanzler der Deutschen Einheit' bezeichnet?",
        "c": [
            "Gerhard Schröder",
            "Helmut Kohl",
            "Konrad Adenauer",
            "Helmut Schmidt"
        ]
    },
    "216": {
        "q": "Welches Symbol ist im Plenarsaal des Deutschen Bundestages zu sehen?   <br><img src='./images/plenarsaal.png'>",
        "c": [
            "der Bundesadler.",
            "die Fahne der Stadt Berlin.",
            "der Reichsadler.",
            "die Reichskrone."
        ]
    },
    "217": {
        "q": "In welchem Zeitraum gab es die Deutsche Demokratische Republik (DDR)?",
        "c": [
            "1919 bis 1927",
            "1933 bis 1945",
            "1945 bis 1961",
            "1949 bis 1990"
        ]
    },
    "218": {
        "q": "Wie viele Bundesländer kamen bei der Wiedervereinigung 1990 zur Bundesrepublik Deutschland hinzu?",
        "c": [
            "4",
            "5",
            "6",
            "7"
        ]
    },
    "219": {
        "q": "Die Bundesrepublik Deutschland hat die Grenzen von heute seit",
        "c": [
            "1933",
            "1949",
            "1971",
            "1990"
        ]
    },
    "220": {
        "q": "Der 27 Januar ist in Deutschland ein offizieller Gedenktag. Woran erinnert dieset Tag?",
        "c": [
            "an das Ende des Zweiten Weltkrieges",
            "an die Verabschiedung des Grundgesetzes",
            "an die Wiedervereinigung Deutschlands",
            "an die Opfer des Nationalsozialismus"
        ]
    },
    "221": {
        "q": "Deutschland ist ein Mitglied des Schengener Abkommens. Was bedeutet das?",
        "c": [
            "Deutsche können in viele Länder Europas ohne Passkontrolle reisen.",
            "Alle Menschen können ohne Personenkontrolle in Deutschland einreisen.",
            "Deutsche können ohne Passkontrolle in jedes Land reisen.",
            "Deutsche können in jedem Land mit dem Euro bezahlen."
        ]
    },
    "222": {
        "q": "Welches Land ist ein Nachbarland von Deutschland?",
        "c": [
            "Ungarn",
            "Portugal",
            "Spanien",
            "Schweiz"
        ]
    },
    "223": {
        "q": "Welches Land ist ein Nachbarland von Deutschland?",
        "c": [
            "Rumänien",
            "Bulgarien",
            "Polen",
            "Griechenland"
        ]
    },
    "224": {
        "q": "Was bedeutet die Abkürzung EU?",
        "c": [
            "Europäische Unternehmen",
            "Europäische Union",
            "Einheitliche Union",
            "Euro Union"
        ]
    },
    "225": {
        "q": "In welchem anderen Land gibt es eine große deutschsprachige Bevölkerung?",
        "c": [
            "Tschechien",
            "Norwegen",
            "Spanien",
            "Österreich"
        ]
    },
    "226": {
        "q": "Welche ist die Flagge der Europäische Union? <br><img src='./images/eu-flag.png'>",
        "c": [
            "1",
            "2",
            "3",
            "4"
        ]
    },
    "227": {
        "q": "Welches Land ist ein Nachbarland von Deutschland?",
        "c": [
            "Finnland",
            "Dänemark",
            "Norwegen",
            "Schweden"
        ]
    },
    "228": {
        "q": "Wie wird der Beitritt der DDR zur Bundesrepublik Deutschland im Jahr 1990 allgemein genannt?",
        "c": [
            "NATO-Osterweiterung",
            "EU-Osterweiterung",
            "Deutsche Wiedervereinigung",
            "Europäische Gemeinschaft"
        ]
    },
    "229": {
        "q": "Welches Land ist ein Nachbarland von Deutschland?",
        "c": [
            "Spanien",
            "Bulgarien",
            "Norwegen",
            "Luxemburg"
        ]
    },
    "230": {
        "q": "Das Europäische Parlament wird regelmäßig gewählt, nämlich alle ...",
        "c": [
            "5 Jahre.",
            "6 Jahre.",
            "7 Jahre.",
            "8 Jahre."
        ]
    },
    "231": {
        "q": "Was bedeutet der Begriff 'europäische Integration'?",
        "c": [
            "Damit sind amerikanische Einwanderer in Europa gemeint.",
            "Der Begriff meint den Einwanderungsstopp nach Europa.",
            "Damit sind europäische Auswanderer in den USA gemeint.",
            "Der Begriff meint den Zusammenschluss europäischer Staaten zur EU."
        ]
    },
    "232": {
        "q": "Wer wird bei der Europawahl gewählt?",
        "c": [
            "die Europäische Kommission",
            "die Länder, die in die EU eintreten dürfen",
            "die Abgeordneten des Europäischen Parlaments",
            "die europäische Verfassung"
        ]
    },
    "233": {
        "q": "Welches Land ist ein Nachbarland von Deutschland?",
        "c": [
            "Tschechien",
            "Bulgarien",
            "Griechenland",
            "Portugal"
        ]
    },
    "234": {
        "q": "Wo ist der Sitz des Europäischen Parlaments?",
        "c": [
            "London",
            "Paris",
            "Berlin",
            "Straßburg"
        ]
    },
    "235": {
        "q": "Der französische Staatspräsident Francois Mitterrand und der deutsche Bundeskanzler Helmut Kohl gedenken in Verdu gemeinsam der Toten beider Weltkriege. Welches Ziel der Europäischen Union wird bei diesem Treffen deutlich? <br><img src='./images/france-germany.png'>",
        "c": [
            "Freundschaft zwischen England und Deutschland",
            "Reisefreiheit in alle Länder der EU",
            "Frieden und Sicherheit in den Ländern der EU",
            "einheitliche Feiertage in den Ländern der EU"
        ]
    },
    "236": {
        "q": "Wie viele Mitgliedstaaten hat die EU heute?",
        "c": [
            "21",
            "23",
            "25",
            "27"
        ]
    },
    "237": {
        "q": "2007 wurde das 50-jährige Jubliäum der 'Römischen Verträge' gefeiert. Was war der Inhalt der Verträge?",
        "c": [
            "Beitritt Deutschlands zur NATO",
            "Gründung der Europäischen Wirtschaftsgemeinschaft (EWG)",
            "Verpflichtung Deutschlands zu Reparationsleistungen",
            "Festlegung der Oder-Neiße-Linie als Ostgrenze"
        ]
    },
    "238": {
        "q": "An welchem Orten arbeitet das Europäische Parlament?",
        "c": [
            "Paris, London und Den Haag",
            "Straßburg, Luxemburg und Brüssel",
            "Rom, Bern und Wien",
            "Bonn, Zürich und Mailand"
        ]
    },
    "239": {
        "q": "Durch welche Verträge schloss sich die Bundesrepublik Deutschland mit anderen Staaten zur Europäischen Wirtschaftsgemeinschaft zusammen?",
        "c": [
            "durch die 'Hamburger Verträge'",
            "durch die 'Römischen Verträge'",
            "durch die 'Pariser Verträge'",
            "durch die 'Londoner Verträge'"
        ]
    },
    "240": {
        "q": "Seit wann bezahlt man in Deutschland mit dem Euro in bar?",
        "c": [
            "1995",
            "1998",
            "2002",
            "2005"
        ]
    },
    "241": {
        "q": "Frau Seger bekommt ein Kind. Was muss sie tun, um Elterngeld zu erhalten?",
        "c": [
            "Sie muss an ihre Krankenkasse schreiben.",
            "Sie muss einen Antrag bei der Elterngeldstelle stellen.",
            "Sie muss nichts tun, denn sie bekommt automatisch Elterngeld.",
            "Sie muss das Arbeitsamt um Erlaubnis bitten."
        ]
    },
    "242": {
        "q": "Wer entscheidet, ob ein Kind in Deutschland in den Kindergarten geht?",
        "c": [
            "der Staat",
            "die Bundesländer",
            "die Eltern/die Erziehungsberechtigten",
            "die Schulen"
        ]
    },
    "243": {
        "q": "Maik und Sybille wollen mit Freunden an ihrem deutschen Wonhort eine Demonstration auf der Straße abhalten. Was müssen sie vorher tun?",
        "c": [
            "Sie müssen die Demonstration anmelden.",
            "Sie müssen nichts tun. Man darf in Deutschland jederzeit überall demonstrieren.",
            "Sie können gar nichts tun, denn Demonstrationen sind in Deutschland grundsätzlich verboten.",
            "Maik und Sybille müssen einen neuen Verein gründen, weil nur Vereine demonstrieren dürfen."
        ]
    },
    "244": {
        "q": "Welchen Schulabschluss braucht man normalerweise, um an einer Universität in Deutschland ein Studium zu beginnen?",
        "c": [
            "das Abitur",
            "ein Diplom",
            "die Prokura",
            "eine Gesellenprüfung"
        ]
    },
    "245": {
        "q": "Wer darf in Deutschland nicht als Paar zusammenleben?",
        "c": [
            "Hans (20 Jahre) und Marie (19 Jahre)",
            "Tom (20 Jahre) und Klaus (45 Jahre)",
            "Sofie (35 Jahre) und Lisa (40 Jahre)",
            "Anne (13 Jahre) und Tim (25 Jahre)"
        ]
    },
    "246": {
        "q": "Ab welchem Alter ist man in Deutschland volljährig?",
        "c": [
            "16",
            "18",
            "19",
            "21"
        ]
    },
    "247": {
        "q": "Eine Frau ist schwanger. Sie ist kurz vor und nach der Geburt ihres Kindes vom Gesetz besonders beschützt. Wie heißt dieser Schutz?",
        "c": [
            "Elternzeit",
            "Mutterschutz",
            "Geburtsvorbereitung",
            "Wochenbett"
        ]
    },
    "248": {
        "q": "Die Erziehung der Kinder in Deutschland ist vor allem Aufgabe",
        "c": [
            "des Staates.",
            "der Eltern.",
            "der Großeltern.",
            "der Schulen."
        ]
    },
    "249": {
        "q": "Wer ist in Deutschland hauptsächlich verantwortlich für die Kindererziehung?",
        "c": [
            "der Staat",
            "die Eltern",
            "die Verwandten",
            "die Schulen"
        ]
    },
    "250": {
        "q": "In Deutschland hat man die besten Chancen auf einen gut bezahlten Arbeitsplatz, wenn man",
        "c": [
            "katholisch ist.",
            "gut ausgebildet ist.",
            "eine Frau ist.",
            "Mitglied einer Partei ist."
        ]
    },
    "251": {
        "q": "Wenn man in Deutschland ein Kind schlägt",
        "c": [
            "geht das niemanden etwas an.",
            "geht das nur die Familie etwas an.",
            "kann man dafür nicht bestraft werden.",
            "kann man dafür bestraft werden."
        ]
    },
    "252": {
        "q": "In Deutschland",
        "c": [
            "darf man zur gleichen Zeit nur mit einem Partner / einer Partnerin verheiratet sein.",
            "kann man mehrere Ehepartner / Ehepartnerinnen gleichzeitig haben.",
            "darf man nicht wieder heiraten, wenn man einmal verheiratet war.",
            "darf eine Frau nicht wieder heiraten, wenn ihr Mann gestorben ist."
        ]
    },
    "253": {
        "q": "Wo müssen Sie sich anmelden, wenn Sie in Deutschland umziehen?",
        "c": [
            "beim Einwohnermeldeamt",
            "beim Standesamt",
            "beim Ordnungsamt",
            "beim Gewerbeamt"
        ]
    },
    "254": {
        "q": "In Deutschland dürfen Ehepaare sich scheiden lassen. Meistens müssen sie dazu das 'Trennungsjahr' einhalten. Was bedeutet das?",
        "c": [
            "Der Scheidungsprozess dauert ein Jahr.",
            "Mann und Frau sind ein Jahr verheiratet, dann ist die Scheidung möglich.",
            "Das Besuchsrecht für die Kinder gilt ein Jahr.",
            "Mann und Frau führen mindestens ein Jahr getrennt ihr eigenes Leben. Danach ist die Scheidung möglich."
        ]
    },
    "255": {
        "q": "Bei Erziehungsproblemen können Eltern in Deutschland Hilfe erhalten vom",
        "c": [
            "Ordnungsamt.",
            "Schulamt.",
            "Jugendamt.",
            "Gesundheitsamt."
        ]
    },
    "256": {
        "q": "Ein Ehepaar möchte in Deutschland ein Restaurant eröffnen. Was braucht es dazu unbedingt?",
        "c": [
            "eine Erlaubnis der Polizei",
            "eine Genehmigung einer Partei",
            "eine Genehmigung des Einwohnermeldeamts",
            "eine Gaststättenerlaubnis von der zuständigen Behörde"
        ]
    },
    "257": {
        "q": "Eine erwachsene Frau möchte in Deutschland das Abitur nachholen. Das kann sie an",
        "c": [
            "einer Hochschule.",
            "einem Abendgymnasium.",
            "einer Hauptschule.",
            "einer Privatuniversität."
        ]
    },
    "258": {
        "q": "Was darf das Jugendamt in Deutschland?",
        "c": [
            "Es entscheidet, welche Schule das Kind besucht.",
            "Es kann ein Kind, das geschlagen wird oder hungern muss, aus der Familie nehmen.",
            "Es bezahlt das Kindergeld an die Eltern.",
            "Es kontrolliert, ob das Kind einen Kindergarten besucht."
        ]
    },
    "259": {
        "q": "Das Berufsinformationszentrum BIZ bei der Bundesagentur für Arbeit in Deutschland hilft bei der",
        "c": [
            "Rentenberechnung.",
            "Lehrstellensuche.",
            "Steuererklärung.",
            "Krankenversicherung."
        ]
    },
    "260": {
        "q": "In Deutschland hat ein Kind in der Schule",
        "c": [
            "Recht auf unbegrenzte Freizeit.",
            "Wahlfreiheit für alle Fächer.",
            "Anspruch auf Schulgeld.",
            "Anwesenheitspflicht."
        ]
    },
    "261": {
        "q": "Ein Mann möchte mit 30 Jahren in Deutschland sein Abitur nachholen. Wo kann er das tun? An",
        "c": [
            "einer Hochschule",
            "einem Abendgymnasium",
            "einer Hauptschule",
            "einer Privatuniversität"
        ]
    },
    "262": {
        "q": "Was bedeutet in Deutschland der Grundsatz der Gleichbehandlung?",
        "c": [
            "Niemand darf z.B. wegen einer Behinderung benachteiligt werden.",
            "Man darf andere Personen benachteiligen, wenn ausreichende persönliche Gründe hierfür vorliegen.",
            "Niemand darf gegen Personen klagen, wenn sie benachteiligt wurden.",
            "Es ist für alle Gesetz, benachteiligten Gruppen jährlich Geld zu spenden."
        ]
    },
    "263": {
        "q": "In Deutschland sind Jugendliche ab 14 Jahren strafmündig. Das bedeutet: Jugendliche, die 14 Jahre und älter sind und gegen Strafgesetze verstoßen, ",
        "c": [
            "werden bestraft.",
            "werden wie Erwachsene behandelt.",
            "teilen die Strafe mit ihren Eltern.",
            "werden nicht bestraft."
        ]
    },
    "264": {
        "q": "Zu welchem Fest tragen Menschen in Deutschland bunte Kostüme und Masken?",
        "c": [
            "am Rosenmontag",
            "am Maifeiertag",
            "beim Oktoberfest",
            "an Pfingsten"
        ]
    },
    "265": {
        "q": "Wohin muss man in Deutschland zuerst gehen, wenn man heiraten möchte?",
        "c": [
            "zum Einwohnermeldeamt",
            "zum Ordnungsamt",
            "zur Agentur für Arbeit",
            "zum Standesamt"
        ]
    },
    "266": {
        "q": "Wann beginnt die gesetzliche Nachtruhe in Deutschland?",
        "c": [
            "wenn die Sonne untergeht",
            "wenn die Nachbarn schlafen gehen",
            "um 0 Uhr, Mitternacht",
            "um 22 Uhr"
        ]
    },
    "267": {
        "q": "Eine junge Frau in Deutschland, 22 Jahre alt, lebt mit ihrem Freund zusammen. Die Eltern der Frau finden das nicht gut, weil ihnen der Freund nicht gefällt. Was können die Eltern tun?",
        "c": [
            "Sie müssen die Entscheidung der volljährigen Tochter respektieren.",
            "Sie haben das Recht, die Tochter in die elterliche Wohnung zurückzuholen.",
            "Sie können zur Polizei gehen und die Tochter anzeigen.",
            "Sie suchen einen anderen Mann für die Tochter."
        ]
    },
    "268": {
        "q": "Eine junge Fraue will den Führerschein machen. Sie hat Angst vor der Prüfung, weil ihre Muttersprache nicht Deutsch ist. Was ist richtig?",
        "c": [
            "Sie muss mindestens zehn Jahre in Deutschland leben, bevor sie den Führerschein machen kann.",
            "Wenn sie kein Deutsch kann, darf sie keinen Führerschein haben.",
            "Sie muss den Führerschein in dem Land machen, in dem man ihre Sprache spricht.",
            "Sie kann die Theorie-Prüfung vielleicht in ihrer Muttersprache machen. Es gibt mehr als zehn Sprachen zur Auswahl."
        ]
    },
    "269": {
        "q": "In Deutschland haben Kinder ab dem Alter von drei Jahren bis zur Ersteinschulung einen Anspruch auf",
        "c": [
            "monatliches Taschengeld.",
            "einen Platz in einem Sportverein.",
            "einen Kindergartenplatz.",
            "einen Ferienpass."
        ]
    },
    "270": {
        "q": "Die Volkshochschule in Deutschland ist eine Einrichtung",
        "c": [
            "für den Religionsunterricht.",
            "nur für Jugendliche.",
            "zur Weiterbildung.",
            "nur für Rentner und Rentnerinnen."
        ]
    },
    "271": {
        "q": "Was ist in Deutschland ein Brauch zu Weihnachten?",
        "c": [
            "bunte Eier verstecken",
            "einen Tannenbaum schmücken",
            "sich mit Masken und Kostümen verkleiden",
            "Kürbisse vor die Tür stellen"
        ]
    },
    "272": {
        "q": "Welche Lebensform ist in Deutschland nicht erlaubt?",
        "c": [
            "Mann und Frau sind geschieden und leben mit neuen Partnern zusammen.",
            "Zwei Frauen leben zusammen.",
            "Ein alleinerziehender Vater lebt mit seinen zwei Kindern zusammen.",
            "Ein Mann ist mit zwei Frauen zur selben Zeit verheiratet."
        ]
    },
    "273": {
        "q": "Bei Erziehungsproblemen gehen Sie in Deutschland",
        "c": [
            "zum Arzt / zur Ärztin.",
            "zum Gesundheitsamt.",
            "zum Einwohnermeldeamt.",
            "zum Jugendamt."
        ]
    },
    "274": {
        "q": "Sie haben in Deutschland absichtlich einen Brief geöffnet, der an eine andere Person adressiert ist. Was haben Sie nicht beachtet?",
        "c": [
            "das Schweigerecht",
            "das Briefgeheimnis",
            "die Schweigepflicht",
            "die Meinungsfreiheit"
        ]
    },
    "275": {
        "q": "Was braucht man in Deutschland für eine Ehescheidung?",
        "c": [
            "die Einwilligung der Eltern",
            "ein Attest eines Arztes / einer Ärztin",
            "die Einwilligung der Kinder",
            "die Unterstützung eines Anwalts / einer Anwältin"
        ]
    },
    "276": {
        "q": "Was sollten Sie tun, wenn Sie von Ihrem Ansprechpartner / Ihrer Ansprechpartnerin in einer deutschen Behörde schlecht behandelt werden?",
        "c": [
            "Ich kann nichts tun.",
            "Ich muss mir diese Behandlung gefallen lassen.",
            "Ich drohe der Person.",
            "Ich kann mich beim Behördenleiter / bei der Behördenleiterin beschweren."
        ]
    },
    "277": {
        "q": "Eine Frau, die ein zweijähriges Kind hat, bewirbt sich in Deutschland um eine Stelle. Was ist ein Beispiel für Diskriminierung? Sie bekommt die Stelle nur deshalb nicht, weil sie",
        "c": [
            "kein Englisch spricht.",
            "zu hohe Gehaltsvorstellungen hat.",
            "keine Erfahrungen in diesem Beruf hat.",
            "Mutter ist."
        ]
    },
    "278": {
        "q": "Ein Mann im Rollstuhl hat sich auf eine Stelle als Buchhalter beworben. Was ist ein Beispiel für Diskriminierung? Er bekommt die Stelle nur deshalb nicht, weil er",
        "c": [
            "im Rollstuhl sitzt.",
            "keine Erfahrung hat.",
            "zu hohe Gehaltsvorstellungen hat.",
            "kein Englisch spricht."
        ]
    },
    "279": {
        "q": "In den meisten Miethäusern in Deutschland gibt es eine 'Hausordnung'. Was steht in einer solchen 'Hausordnung'? Sie nennt",
        "c": [
            "Regeln für die Benutzung öffentlicher Verkehrsmittel.",
            "alle Mieter und Mieterinnen im Haus.",
            "Regeln, an die sich alle Bewohner und Bewohnerinnen halten müssen.",
            "die Adresse des nächsten Ordnungsamtes."
        ]
    },
    "280": {
        "q": "Wenn Sie sich in Deutschland gegen einen falschen Steuerbescheid wehren wollen, müssen Sie",
        "c": [
            "nichts machen.",
            "den Bescheid wegwerfen.",
            "Einspruch einlegen.",
            "warten, bis ein anderer Bescheid kommt."
        ]
    },
    "281": {
        "q": "Zwei Freunde wollen in ein öffentliches Schwimmbad in Deutschland Beide haben eine dunkle Hautfarbe und werden deshalb nicht hineingelassen. Welches Recht wird in dieser Situation verletzt? Das Recht auf",
        "c": [
            "Meinungsfreiheit.",
            "Gleichbehandlung.",
            "Versammlungsfreiheit.",
            "Freizügigkeit."
        ]
    },
    "282": {
        "q": "Welches Ehrenamt müssen deutsche Staatsbürger / Staatsbürgerinnen übernehmen, wenn sie dazu aufgefordert werden?",
        "c": [
            "Vereinstrainer / Vereinstrainerin",
            "Wahlhelfer / Wahlhelferin",
            "Bibliotheksaufsicht",
            "Lehrer / Lehrerin"
        ]
    },
    "283": {
        "q": "Was tun Sie, wenn Sie eine falsche Rechnung von einer deutschen Behörde bekommen?",
        "c": [
            "Ich lasse die Rechnung liegen.",
            "Ich lege Widerspruch bei der Behörde ein.",
            "Ich schicke die Rechnung an die Behörde zurück.",
            "Ich gehe mit der Rechnung zum Finanzamt."
        ]
    },
    "284": {
        "q": "Was man für die Arbeit können muss, ändert sich in Zukunft sehr schnell. Was kann man tun?",
        "c": [
            "Es ist egal, was man lernt.",
            "Erwachsene müssen auch nach der Ausbildung immer weiter lernen.",
            "Kinder lernen in der Schule alles, was im Beruf wichtig ist. Nach der Schule muss man nicht weiter lernen.",
            "Alle müssen früher aufhören zu arbeiten, weil sich alles ändert."
        ]
    },
    "285": {
        "q": "Frau Frost arbeitet als fest angestellte Mitarbeiterin in einem Büro. Was muss sie nicht von ihrem Gehalt bezahlen?",
        "c": [
            "Lohnsteuer",
            "Beiträge zur Arbeitslosenversicherung",
            "Beiträge zur Renten- und Krankenversicherung",
            "Umsatzsteuer"
        ]
    },
    "286": {
        "q": "Welche Organisation in einer Firma hilft den Arbeitnehmern und Arbeitnehmerinnen bei Problemen mit dem Arbeitgeber / der Arbeitgeberin?",
        "c": [
            "der Betriebsrat",
            "der Betriebsprüfer / die Betriebsprüferin",
            "die Betriebsgruppe",
            "das Betriebsmanagement"
        ]
    },
    "287": {
        "q": "Sie möchten bei einer Firma in Deutschland Ihr Arbeitsverhältnis beenden. Was müssen Sie beachten?",
        "c": [
            "die Gehaltszahlungen",
            "die Arbeitszeit",
            "die Kündigungsfrist",
            "die Versícherungspflicht"
        ]
    },
    "288": {
        "q": "Bei welchem Amt muss man in Deutschland in der Regel seinen Hund anmelden?",
        "c": [
            "beim Finanzamt",
            "beim Einwohnermeldeamt",
            "bei der Kommune (Stadt oder Gemeinde)",
            "beim Gesundheitsamt"
        ]
    },
    "289": {
        "q": "Ein Mann mit dunkler Hautfarbe bewirbt sich um eine Stelle als Kellner in einem Restaurant in Deutschland. Was ist ein Beispiel für Diskriminierung? Er bekommt die Stelle nur deshalb nicht, weil",
        "c": [
            "seine Deutschkenntnisse zu gering sind.",
            "er zu hohe Gehaltsvorstellungen hat.",
            "er eine dunkle Haut hat.",
            "er keine Erfahrungen im Beruf hat."
        ]
    },
    "290": {
        "q": "Sie haben in Deutschland einen Fernseher gekauft. Zu Hause packen Sie den Fernseher aus, doch er funktioniert nicht. Der Fernseher ist kaputt. Was können Sie machen?",
        "c": [
            "eine Anzeige schreiben",
            "den Fernseher reklamieren",
            "das Gerät ungefragt austauschen",
            "die Garantie verlängern"
        ]
    },
    "291": {
        "q": "Warum muss man in Deutschland bei der Steuererklärung aufschreiben, ob man zu einer Kirche gehört oder nicht? Weil",
        "c": [
            "es eine Kirchensteuer gibt, die an die Einkommen- und Lohnsteuer geknüpft ist.",
            "das für die Statistik in Deutschland wichtig ist.",
            "man mehr Steuern zahlen muss, wenn man nicht zu einer Kirche gehört.",
            "die Kirche für die Steuererklärung verantwortlich ist."
        ]
    },
    "292": {
        "q": "Die Menschen in Deutschland leben nach dem Grundsatz der religiösen Toleranz. Was bedeutet das?",
        "c": [
            "Es dürfen keine Moscheen gebaut werden.",
            "Alle Menschen glauben an Gott.",
            "Jeder kann glauben, was er möchte.",
            "Der Staat entscheidet, an welchen Gott die Menschen glauben."
        ]
    },
    "293": {
        "q": "Was ist in Deutschland ein Brauch an Ostern?",
        "c": [
            "Kürbisse vor die Tür stellen",
            "einen Tannenbaum schmücken",
            "Eier bemalen",
            "Raketen in die Luft schießen"
        ]
    },
    "294": {
        "q": "Pfingsten ist ein",
        "c": [
            "christlicher Feiertag.",
            "deutscher Gedenktag.",
            "internationaler Trauertag.",
            "bayerischer Brauch."
        ]
    },
    "295": {
        "q": "Welche Religion hat die europäische und deutsche Kultur geprägt?",
        "c": [
            "der Hinduismus",
            "das Christentum",
            "der Buddhismus",
            "der Islam"
        ]
    },
    "296": {
        "q": "In Deutschland nennt man die letzten vier Wochen vor Weihnachten",
        "c": [
            "den Buß- und Bettag.",
            "das Erntedankfest.",
            "die Adventszeit.",
            "Allerheiligen."
        ]
    },
    "297": {
        "q": "Aus welchem Land sind die meisten Migranten / Migrantinnen nach Deutschland gekommen?",
        "c": [
            "Italien",
            "Polen",
            "Marokko",
            "Türkei"
        ]
    },
    "298": {
        "q": "In der DDR lebten vor allem Migranten aus",
        "c": [
            "Vietnam, Polen, Mosambik.",
            "Frankreich, Rumänien, Somalia.",
            "Chile, Ungarn, Simbabwe.",
            "Nordkorea, Mexiko, Ägypten."
        ]
    },
    "299": {
        "q": "Ausländische Arbeitnehmer und Arbeitnehmerinnen, die in den 50er- und 60er Jahren von der Bundesrepublik Deutschland angeworben wurden, nannte man",
        "c": [
            "Schwarzarbeiter / Schwarzarbeiterinnen.",
            "Gastarbeiter / Gastarbeiterinnen.",
            "Zeitarbeiter /Zeitarbeiterinnen.",
            "Schichtarbeiter / Schichtarbeiterinnen."
        ]
    },
    "300": {
        "q": "Aus welchem Land kamen die ersten Gastarbeiter / Gastarbeiterinnen in die Bundesrepublik Deutschland?",
        "c": [
            "Italien",
            "Spanien",
            "Portugal",
            "Türkei"
        ]
    },

    // Baden-Württemberg
    "301": {
        "q": "",
        "c": [
            ""
        ]
    },
    "302": {
        "q": "",
        "c": [
            ""
        ]
    },
    "303": {
        "q": "",
        "c": [
            ""
        ]
    },
    "304": {
        "q": "",
        "c": [
            ""
        ]
    },
    "305": {
        "q": "",
        "c": [
            ""
        ]
    },
    "306": {
        "q": "",
        "c": [
            ""
        ]
    },
    "307": {
        "q": "",
        "c": [
            ""
        ]
    },
    "308": {
        "q": "",
        "c": [
            ""
        ]
    },
    "309": {
        "q": "",
        "c": [
            ""
        ]
    },
    "310": {
        "q": "",
        "c": [
            ""
        ]
    },

    // Bayern
    "311": {
        "q": "Welches Wappen gehört zum Freistaat Bayern? <br><img src='./images/bavaria-flag.png'>",
        "c": [
            "1",
            "2",
            "3",
            "4"
        ]
    },
    "312": {
        "q": "Welches ist ein Landkreis in Bayern?",
        "c": [
            "Prignitz",
            "Rhein-Sieg-Kreis",
            "Nordfriesland",
            "Altötting"
        ]
    },
    "313": {
        "q": "Für wie viele Jahre wird der Landtag in Bayern gewählt?",
        "c": [
            "3",
            "4",
            "5",
            "6"
        ]
    },
    "314": {
        "q": "Ab welchem Alter darf man in Bayern bei Kommunalwahlen wählen?",
        "c": [
            "14",
            "16",
            "18",
            "20"
        ]
    },
    "315": {
        "q": "Welche Farben hat die Landesflagge von Bayern?",
        "c": [
            "blau-weiß-rot",
            "weiß-blau",
            "grün-weiß-rot",
            "schwarz-gelb"
        ]
    },
    "316": {
        "q": "Wo können Sie sich in Bayern über politische Themen informieren?",
        "c": [
            "beim Ordnungsamt der Gemeinde",
            "bei der Landeszentrale für politische Bildung",
            "bei der Verbraucherzentrale",
            "bei den Kirchen"
        ]
    },
    "317": {
        "q": "Die Landeshauptstadt von Bayern heißt",
        "c": [
            "Ingolstadt.",
            "Regensburg.",
            "Nürnberg.",
            "München."
        ]
    },
    "318": {
        "q": "Welches Bundesland ist Bayern? <br><img src='./images/bavaria-map.png'>",
        "c": [
            "1",
            "2",
            "3",
            "4"
        ]
    },
    "319": {
        "q": "Wie nennt man den Regierungschef / die Regierungschefin in Bayern?",
        "c": [
            "Erster Minister/ Erste Ministerin",
            "Premierminister/ Premierministerin",
            "Bürgermeister/ Bürgermeisterin",
            "Ministerpräsident/ Ministerpräsidentin"
        ]
    },
    "320": {
        "q": "Welchen Minister / welche Ministerin hat Bayern nicht?",
        "c": [
            "Justizminister/ Justizministerin",
            "Außenminister/ Außenministerin",
            "Finanzminister/ Finanzministerin",
            "Innenminister/ Innenministerin"
        ]
    },

    // Berlin
    "321": {
        "q": "",
        "c": [
            ""
        ]
    },
    "322": {
        "q": "",
        "c": [
            ""
        ]
    },
    "323": {
        "q": "",
        "c": [
            ""
        ]
    },
    "324": {
        "q": "",
        "c": [
            ""
        ]
    },
    "325": {
        "q": "",
        "c": [
            ""
        ]
    },
    "326": {
        "q": "",
        "c": [
            ""
        ]
    },
    "327": {
        "q": "",
        "c": [
            ""
        ]
    },
    "328": {
        "q": "",
        "c": [
            ""
        ]
    },
    "329": {
        "q": "",
        "c": [
            ""
        ]
    },
    "330": {
        "q": "",
        "c": [
            ""
        ]
    },

    // Brandenburg
    "331": {
        "q": "",
        "c": [
            ""
        ]
    },
    "332": {
        "q": "",
        "c": [
            ""
        ]
    },
    "333": {
        "q": "",
        "c": [
            ""
        ]
    },
    "334": {
        "q": "",
        "c": [
            ""
        ]
    },
    "335": {
        "q": "",
        "c": [
            ""
        ]
    },
    "336": {
        "q": "",
        "c": [
            ""
        ]
    },
    "337": {
        "q": "",
        "c": [
            ""
        ]
    },
    "338": {
        "q": "",
        "c": [
            ""
        ]
    },
    "339": {
        "q": "",
        "c": [
            ""
        ]
    },
    "340": {
        "q": "",
        "c": [
            ""
        ]
    },

    // Bremen
    "341": {
        "q": "",
        "c": [
            ""
        ]
    },
    "342": {
        "q": "",
        "c": [
            ""
        ]
    },
    "343": {
        "q": "",
        "c": [
            ""
        ]
    },
    "344": {
        "q": "",
        "c": [
            ""
        ]
    },
    "345": {
        "q": "",
        "c": [
            ""
        ]
    },
    "346": {
        "q": "",
        "c": [
            ""
        ]
    },
    "347": {
        "q": "",
        "c": [
            ""
        ]
    },
    "348": {
        "q": "",
        "c": [
            ""
        ]
    },
    "349": {
        "q": "",
        "c": [
            ""
        ]
    },
    "350": {
        "q": "",
        "c": [
            ""
        ]
    }

    // Hamburg

    // Hessen

    // Mecklenburg-Vorpommern

    // Niedersachsen

    // Nordrhein-Westfalen

    // Rheinland-Pfalz

    // Saarland

    // Sachsen

    // Sachsen-Anhalt

    // Schleswig-Holstein

    // Thüringen

};
